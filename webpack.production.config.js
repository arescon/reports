const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const WebpackAutoInject = require('webpack-auto-inject-version');
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

const srcPath = path.resolve(__dirname, 'src');
const outPath = path.resolve(__dirname, 'dist/');

const sassOptions = {
  sourceMap: true,
  // Автовключение variables.scss для всех scss
  data: '@import "variables";',
  includePaths: [
    path.resolve(__dirname, './src/styles/'),
  ],
};


module.exports = {
  context: srcPath,


  entry: {
    index: [
      'babel-polyfill',
      './index.js',
    ],
  },


  module: {
    rules: [

      {
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/,
        use: [{
          loader: 'babel-loader',
          options: {
            presets: ['react', 'es2015', 'stage-0', 'flow'],
            plugins: [
              'react-html-attrs',
              'transform-class-properties',
              'transform-decorators-legacy',
              'transform-runtime',
              'transform-flow-strip-types',
            ],
          },
        }],
      },

      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              modules: true,
              sourceMap: false,
            },
          },
          'postcss-loader',
        ],
      },

      {
        test: /\.(scss|sass)$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              modules: true,
              importLoaders: 1,
              localIdentName: '[hash:base64:7]',
              sourceMap: false,
            },
          },
          'postcss-loader',
          'resolve-url-loader',
          {
            loader: 'sass-loader',
            options: sassOptions,
          },
        ],
      },

      {
        test: /\.(scss|sass)$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              modules: true,
              importLoaders: 1,
              localIdentName: '[hash:base64:7]',
              sourceMap: false,
            },
          },
          'postcss-loader',
          'resolve-url-loader',
          {
            loader: 'sass-loader',
            options: sassOptions,
          },
        ],
      },

      {
        test: /\.(png|jpg|svg|gif)$/,
        include: imagesPaths,
        use: [{
          loader: 'file-loader',
          options: {
            name: 'images/[name].[ext]',
          },
        }],
      },

      {
        test: /favicon.png$/,
        include: path.resolve(__dirname, 'app/favicon.png'),
        use: [{
          loader: 'file-loader',
          options: {
            name: './[name].[ext]',
          },
        }],
      },

      {
        test: /\.svg$/,
        exclude: /loader-clock.svg$/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: ['es2015', 'react'],
            },
          },
          'svg-react-loader',
        ],
      },

      // {
      //   test: /\.(ttf|woff|woff2|eot)$/,
      //   use: [{
      //     loader: 'file-loader',
      //     options: {
      //       name: 'fonts/[name].[ext]',
      //     },
      //   }],
      // },

      {
        test: /\.txt$/,
        use: ['raw-loader'],
      },

    ],
  },


  output: {
    path: outPath,
    filename: '[name].min.js',
    publicPath: '/',
  },


  resolve: {
    modules: [path.resolve('src/components'), 'node_modules'],
    alias: {
      src: srcPath,
    },
  },

  optimization: {
    minimizer: [
      new UglifyJsPlugin({
        cache: true,
        parallel: true,
        sourceMap: false,
      }),
      new OptimizeCSSAssetsPlugin({}),
    ],
  },

  plugins: [
    new webpack.ContextReplacementPlugin(/moment[\\\/]locale$/, /^\.\/(ru)$/),
    new webpack.DefinePlugin({ 'process.env': {
      NODE_ENV: JSON.stringify('production'),
      APP_NAME: JSON.stringify('food'),
    },
    }),
    new MiniCssExtractPlugin({
      filename: 'styles[id].css',
    }),
    new HtmlWebpackPlugin({
      template: './common/templates/index.wp4.ejs',
      inject: false,
      minify: {
        collapseWhitespace: true,
        removeComments: true,
      },
      hash: true,
    }),
    new WebpackAutoInject({
      components: {
        AutoIncreaseVersion: true,
        InjectAsComment: true,
        InjectByTag: true,
      },
      componentsOptions: {
        InjectAsComment: {
          tag: 'Version: {version}prod — {date}',
          dateFormat: 'dd.mm.yyyy HH:MM:ss Z',
        },
        InjectByTag: {
          fileRegex: /\.min\.js$/,
          dateFormat: 'dd.mm.yyyy HH:MM:ss Z',
        },
      },
    }),
    new CompressionPlugin({
      asset: '[path].gz[query]',
      algorithm: 'gzip',
      test: /\.js$|\.html$|\.css$/,
      threshold: 10240,
      minRatio: 0.8,
    }),
  ],
  // devtool: 'inline-source-map',
};


// const { resolve } = require('path');
// const Dotenv = require('dotenv-webpack');
// const webpack = require('webpack');
// const HtmlWebpackPlugin = require('html-webpack-plugin');
// const ExtractTextPlugin = require('extract-text-webpack-plugin');
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
// const CompressionPlugin = require("compression-webpack-plugin");

// const sassOptions = {
//   sourceMap: false,
//   // Автовключение variables.scss для всех scss
//   data: '@import "variables";',
//   includePaths: [
//     resolve(__dirname, './src/styles/'),
//   ],
// };

// module.exports = {
//   context: resolve(__dirname, 'src'),
//   entry: {
//     app: './',
//     vendor: ['react', 'moment']
//   },
//   output: {
//     filename: 'reports_react.js',
//     path: resolve(__dirname, 'build'),
//     publicPath: '/',
//   },
//   module: {
//     rules: [
//       {
//         test: /\.(js|jsx)$/,
//         loaders: ['babel-loader'],
//         exclude: /node_modules/
//       },
//       {
//         test: /\.css$/,
//         loader: 'style-loader!css-loader!postcss-loader'
//       },
//       {
//         test: /\.(scss|sass)$/,
//         use: [
//           'style-loader',
//           {
//             loader: 'css-loader',
//             options: {
//               modules: true,
//               importLoaders: 1,
//               localIdentName: '[name]__[local]___[hash:base64:5]',
//               sourceMap: true,
//             },
//           },
//           'postcss-loader',
//           'resolve-url-loader',
//           {
//             loader: 'sass-loader',
//             options: sassOptions,
//           },
//         ],
//       },
//       { test: /\.(png|jpg|gif)$/, use: 'url-loader?limit=15000&name=ocalIdentName=[local].[ext]' },
//       { test: /\.eot(\?v=\d+.\d+.\d+)?$/, use: 'file-loader' },
//       { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, use: 'url-loader?limit=10000&mimetype=application/font-woff' },
//       { test: /\.[ot]tf(\?v=\d+.\d+.\d+)?$/, use: 'url-loader?limit=10000&mimetype=application/octet-stream' },
//       { test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, use: 'url-loader?limit=10000&mimetype=image/svg+xml' }
//     ]
//   },
//   resolve: {
//     extensions: ['.js', '.jsx', '.scss'],
//     alias: {
//       src: resolve('./src')
//     }
//   },
//   plugins: [
//     new webpack.ProvidePlugin({
//       jQuery: 'jquery',
//       $: 'jquery',
//       jquery: 'jquery'
//     }),
//     new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
//     new HtmlWebpackPlugin({
//       template: `${__dirname}/src/index.html`,
//       filename: 'index.html',
//       inject: 'body',
//     }),
//     new webpack.LoaderOptionsPlugin({
//       minimize: false,
//       debug: false,
//     }),
//     new webpack.optimize.ModuleConcatenationPlugin(),
//     new webpack.optimize.OccurrenceOrderPlugin(),
//     new webpack.optimize.UglifyJsPlugin(),
//     new webpack.optimize.CommonsChunkPlugin({
//       name: 'vendor',
//       children: true,
//       async: true
//     })
//     //new BundleAnalyzerPlugin()
//   ]
// }