var { resolve } = require('path');

const sassOptions = {
  sourceMap: false,
  // Автовключение variables.scss для всех scss
  data: '@import "variables";',
  includePaths: [
    resolve(__dirname, './src/styles/'),
  ],
};

module.exports = {
  mode: 'development', // production or development
  entry: './src/index.js',
  output: {
    filename: 'reports_react.js',
    path: resolve(__dirname, 'build'),
    publicPath: '/',
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        loaders: ['babel-loader'],
        exclude: /node_modules/
      },
      {
        test: /\.css$/,
        loader: 'style-loader!css-loader!postcss-loader'
      },
      {
        test: /\.(scss|sass)$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              modules: true,
              importLoaders: 1,
              localIdentName: '[name]__[local]___[hash:base64:5]',
              sourceMap: true,
            },
          },
          'postcss-loader',
          'resolve-url-loader',
          {
            loader: 'sass-loader',
            options: sassOptions,
          },
        ],
      },
      { test: /\.(png|jpg|gif)$/, use: 'url-loader?limit=15000&name=ocalIdentName=[local].[ext]' },
      { test: /\.eot(\?v=\d+.\d+.\d+)?$/, use: 'file-loader' },
      { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, use: 'url-loader?limit=10000&mimetype=application/font-woff' },
      { test: /\.[ot]tf(\?v=\d+.\d+.\d+)?$/, use: 'url-loader?limit=10000&mimetype=application/octet-stream' },
      { test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, use: 'url-loader?limit=10000&mimetype=image/svg+xml' }
    ]
  },
  resolve: {
    extensions: ['.js', '.jsx', '.scss'],
    alias: {
      src: resolve('./src'),
      actions: resolve('./src/redux/actions')
    }
  },
};