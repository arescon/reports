import React from 'react';
import { Provider } from 'react-redux';
import { Route } from 'react-router';
import { HashRouter } from 'react-router-dom';

import { ConnectedRouter } from 'react-router-redux';
import { store, history } from './redux/configStore';

import App from './App';

const RouterBox = () => 
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <HashRouter>
        <Route component={App} />
      </HashRouter>
    </ConnectedRouter>
  </Provider>

export default RouterBox;