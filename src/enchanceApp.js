import { connect } from 'react-redux';

import compose from 'recompose/compose';
import lifecycle from 'recompose/lifecycle';
import withState from 'recompose/withState';
import withHandlers from 'recompose/withHandlers';

import { get_roles, get_services } from './redux/actions/home';
import { set_title, get_menu } from './redux/actions/menu';

export default compose(
  connect(
    state => ({
      error: state.home.error,
      full_app: state.home.full_app,
      page_settings: state.home.page_settings,
      menu: state.menu.menu,
      mo: state.home.mo,
      state: state
    })
  ),
  withState('current_menu', 'current_menu_change', null),
  withState('ready', 'ready_change', false),
  withHandlers({
    fetchData: ({ dispatch }) => () => {
      dispatch(get_services());
      dispatch(get_roles());
      dispatch(get_menu());
    },
    titleChange: ({ dispatch }) => () => {
      dispatch(set_title(this.props.history));
    },
    handlerRefreshReports: ({ dispatch }) => () => {
      dispatch(get_menu());
    },
    handlerCloseReports: () => () => { // закрыть всю модалку отчеты!
      // console.log('close modal reports');
      let o = document.getElementById('reports_react_dds');
      o.classList.remove('reports_show');
    }
  }),
  lifecycle({
    componentDidMount() {
      const { current_menu_change, location } = this.props;
      current_menu_change(location.pathname.split('/')[1]);
      this.props.fetchData();

    },
    componentDidUpdate(prevProps) {
      const { menu, ready, ready_change, current_menu, current_menu_change, location, match } = this.props;
      let url = location.pathname.split('/')[1];
      if(menu.length > 0 && !ready ) {
        ready_change(true);
      }
      if(current_menu !== url) {
        current_menu_change(url);
      }
    }
  })
);