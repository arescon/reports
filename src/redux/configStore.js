import { applyMiddleware, createStore, compose } from 'redux';
import thunk from 'redux-thunk';

import { routerMiddleware } from 'react-router-redux';
import createHistory from 'history/createBrowserHistory';

import reducers from './reducers';

export const history = createHistory();

function configureStore() {
  return createStore(reducers, applyMiddleware(thunk, routerMiddleware(history)));
}

export const store = configureStore();