import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import menu from './menu';
import home from './home';
import settings from './settings';
import modal from './modal';

const appReducer = combineReducers({
  router: routerReducer,
  menu,
  modal,
  home,
  settings
});

export default (state, action) => {
  return appReducer(state, action);
}