import {
  GET_ROLES,
  GET_MO,
  ERROR,
  LOADER,
  PAGE_CHANGE,
  FULL_APP,
  RES_OTCHET
} from '../actions/home';

const initialState = {
  roles: [],
  serv: [],
  mo: [],
  res_otchet: null,
  error: '',
  full_app: true,
  loader: true,
  page_settings: false
};

const handlers = (state = initialState, action) => {
  switch (action.type) {
    case GET_ROLES: {
      return { ...state, roles: action.payload };
    }
    case GET_MO: {
      return { ...state, mo: action.payload };
    }
    case ERROR: {
      return { ...state, error: action.payload };
    }
    case LOADER: {
      return { ...state, loader: !state.loader };
    }
    case RES_OTCHET: {
      return { ...state, res_otchet: action.payload };
    }
    case PAGE_CHANGE: {
      return { ...state, [`${action.payload.u}`]: action.payload.v }
    }
    case FULL_APP: {
      return { ...state, full_app: !state.full_app }
    }
    default:
      return state;
  }
}

export default handlers