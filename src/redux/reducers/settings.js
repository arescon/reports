import {
  GET_MENU_SETTINGS,
  ERROR_MENU_SETTINGS,
  GET_MENU_REPORTS,
  GET_MENU_ONE,
  GET_MENU_REPORTS_ONE,
  CHANGE_MENU_ID,
  MENU_RESET,
  REPORTS_RESET,
} from '../actions/settings';

let initialState = {
  menu: [],
  reports: [],
  report_one: {},
  menu_one: {},
  roles: [],
  error: '',
  menu_id: null
};

const handlers = (state = initialState, action) => {
  switch (action.type) {
    case GET_MENU_SETTINGS: {
      return { ...state, menu: action.payload };
    }
    case GET_MENU_ONE: {
      return { ...state, menu_one: action.payload };
    }
    case ERROR_MENU_SETTINGS: {
      return { ...state, error: action.payload };
    }
    case GET_MENU_REPORTS_ONE: {
      return { ...state, report_one: action.payload };
    }
    case GET_MENU_REPORTS: {
      return {...state, reports: action.payload }
    }
    case CHANGE_MENU_ID: {
      return {...state, menu_id: action.payload }
    }
    case MENU_RESET: {
      return {...state, menu: [] }
    }
    case REPORTS_RESET: {
      return {...state, reports: [] }
    }
    default:
      return state;
  }
}

export default handlers