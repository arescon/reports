import {
  GET_MENU,
  ERROR_MENU
} from '../actions/menu';

let initialState = {
  menu: [],
  error: ''
};

const handlers = (state = initialState, action) => {
  switch (action.type) {
    case GET_MENU: {
      return { ...state, menu: action.payload };
    }
    case ERROR_MENU: {
      return { ...state, error: action.payload };
    }
    default:
      return state;
  }
}

export default handlers