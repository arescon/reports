import {
  CHANGE_MODAL
} from 'actions/modal';
  
let initialState = {
  id: null
};
  
const handlers = (state = initialState, action) => {
  switch (action.type) {
    case CHANGE_MODAL: {
      return { ...state, id: action.payload };
    }
    default:
      return state;
  }
}
  
export default handlers