export const GET_ROLES = 'GET_ROLES';
export const GET_MO = 'GET_MO';
export const LOADER = 'LOADER';
export const ERROR = 'ERROR';
export const FULL_APP = 'FULL_APP';
export const RES_OTCHET = 'RES_OTCHET';
export const PAGE_CHANGE = 'PAGE_CHANGE';

const roles_data = payload => ({
  type: GET_ROLES,
  payload
});

const mo_data = payload => ({
  type: GET_MO,
  payload
});

const otchet_result = payload => ({
  type: RES_OTCHET,
  payload
});

const roles_error = payload => ({
  type: ERROR,
  payload
});

const loader = () => ({
  type: LOADER
});

const page_c = payload => ({
  type: PAGE_CHANGE,
  payload
});

const app_change = () => ({
  type: FULL_APP
});

export const changeFullApp = () => {
  return (dispatch) => {
    dispatch(app_change())
  }
}

export const get_services = () => {
  return (dispatch) => {
    Un.execUsp('reports_reports_service', {}, {}, (res) => {
      dispatch(page_change('serv', res.Data ));
    }, () => {
      dispatch(roles_error('Ошибка загрузки списка сервисов'));
    })
  }
}

export const get_roles = () => {
  return (dispatch) => {
    Un.execUsp('dbo_rmis_GetUserRolesById_admin', {
      operid: '_OPERID_',
      userid: '_OPERID_'
    }, {}, (res) => {
      let arr = res.Data;
      dispatch(roles_data(arr))
      if(arr.length > 0) {
        let f = arr.filter(r => r.id === 37 );
        if(f.length > 0) {
          if(f[0].checked === 1) dispatch(page_change('page_settings', true))
        }
      }
    }, ()=>{
      dispatch(roles_error('Ошибка загрузки ролей'))
    })

    dispatch(loader());
    dispatch(get_all_MO())
  }
}

export const get_all_MO = () => {
  return (dispatch) => {
    Ajax.go('get', '/api/mo/GetAllMo', { query: '' }, {
      s: function (ajd) {
        dispatch(mo_data(ajd))
      }
    });
  }
}

export const page_change = (url, val) => {
  return (dispatch) => {
    dispatch(page_c({
      u: url,
      v: val
    }))
  }
}

export const getOtchet = (data) => {
  return (dispatch) => {
    dispatch(otchet_result(data))
  }
}