import * as R from 'ramda';

export const SET_TITLE = 'SET_TITLE';

export const GET_MENU_SETTINGS = 'GET_MENU_SETTINGS';
export const GET_MENU_ONE = 'GET_MENU_ONE';
export const GET_MENU_REPORTS = 'GET_MENU_REPORTS';
export const GET_MENU_REPORTS_ONE = 'GET_MENU_REPORTS_ONE';
export const ERROR_MENU_SETTINGS = 'ERROR_MENU_SETTINGS';
export const CHANGE_MENU_ID = 'CHANGE_MENU_ID';
export const MENU_RESET = 'MENU_RESET';
export const REPORTS_RESET = 'REPORTS_RESET';

const title = payload => ({
  type: SET_TITLE,
  payload
});

const menu_data = payload => ({
  type: GET_MENU_SETTINGS,
  payload
});

const reports_data = payload => ({
  type: GET_MENU_REPORTS,
  payload
});

const reports_data_one = payload => ({
  type: GET_MENU_REPORTS_ONE,
  payload
});

const menu_id = payload => ({
  type: CHANGE_MENU_ID,
  payload
})

const menu_data_one = payload => ({
  type: GET_MENU_ONE,
  payload
});

const menu_error = payload => ({
  type: ERROR_MENU_SETTINGS,
  payload
})

export const set_title = (history) => {
  return (dispatch) => {
    dispatch(title())
  }
}

export const get_menu = () => {
  return (dispatch) => {
    Un.execUsp('reports_menu_reports_select', {
      aspoper: '_OPERID_',
      substr: '',
      page_num: 1,
      page_size: null
    }, {}, (res) => {
      dispatch(menu_data(res.Data))
    }, ()=>{
      dispatch(menu_error('Ошибка загрузки меню'))
    })
  }
}

export const get_menu_one = (_id) => {
  return (dispatch) => {
    Un.execUsp('reports_menu_reports_select', {
      id: _id,
      aspoper: '_OPERID_',
      substr: '',
      page_num: 1,
      page_size: null
    }, {}, (res) => {
      if(_id !== 0) {
        let byId = R.ascend(R.prop('id'));
        dispatch(menu_data_one(R.sort(byId, res.Data[0] || {})));
      };
    }, ()=>{
      dispatch(menu_data_one({}));
      dispatch(menu_error('Ошибка загрузки меню'))
    })
  }
}

export const get_reports_menu = (id_menu) => {
  return (dispatch) => {
    Un.execUsp('reports_reports_proc_select', {
      menu_reports_pk: id_menu,
      aspoper: '_OPERID_',
      substr: '',
      page_num: 1,
      page_size: null
    }, {}, (res) => {
      let byId = R.ascend(R.prop('id'));
      dispatch(reports_data(R.sort(byId, res.Data || {})));
    }, ()=>{
      dispatch(menu_error('Ошибка загрузки отчетов меню'))
    })
  }
}

export const get_reports_one = (_id, id_menu) => {
  return (dispatch) => {
    // console.log('get one');
    Un.execUsp('reports_reports_proc_select', {
      id: _id,
      menu_reports_pk: id_menu,
      aspoper: '_OPERID_',
      substr: '',
      page_num: 1,
      page_size: null
    }, {}, (res) => {
      // console.log('get one api')
      if(_id !== 0) dispatch(reports_data_one(res.Data[0] || {}));
    }, ()=>{
      dispatch(reports_data_one({}));
      dispatch(menu_error('Ошибка загрузки меню'))
    })
  }
}

export const change_menu_id = (id) => {
  return (dispatch) => {
    dispatch(menu_id(id));
  }
}

export const reset_menu = () => {
  return (dispatch) => {
    dispatch(() => ({type: MENU_RESET}))
  }
}
export const reset_reports = () => {
  return (dispatch) => {
    dispatch(() => ({type: REPORTS_RESET}))
  }
}

export const reset_report_one = () => {
  return (dispatch) => {
    dispatch(reports_data_one({}));
  }
}
// reports_menu_reports_select