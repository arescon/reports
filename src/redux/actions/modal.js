export const CHANGE_MODAL = 'CHANGE_MODAL';

export function toggleModal(id) {
  return (dispatch) => {
    dispatch({
      type: CHANGE_MODAL,
      payload: id
    })
  }
}