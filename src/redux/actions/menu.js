export const SET_TITLE = 'SET_TITLE';

export const GET_MENU = 'GET_MENU';
export const ERROR_MENU = 'ERROR_MENU';

const title = payload => ({
  type: SET_TITLE,
  payload
});

const menu_data = payload => ({
  type: GET_MENU,
  payload
});

const menu_error = payload => ({
  type: ERROR_MENU,
  payload
})

export const set_title = (history) => {
  return (dispatch) => {
    dispatch(title())
  }
}

export const get_menu = () => {
  return (dispatch) => {
    Un.execUsp('reports_reports_all', {
      aspoper: '_OPERID_',
    }, {}, (res) => {
      dispatch(menu_data(res.Data))
    }, ()=>{
      dispatch(menu_error('Ошибка загрузки меню'))
    })
  }
}