import * as R from 'ramda';

// 
export const DICTIONS = [
  {
    id: 'medorg',
    text: 'Мед организации',
    redux: 'mo',
    api_id: 'ID',
    api_val: 'Nam_mok'
  }
];

export const CONFIG = {
  period: {
    title: 'Период',
    active: false,
    beg_data: 'dbeg',
    end_data: 'dend',
  },
  year: {
    title: 'Год',
    active: false,
    label: 'year'
  },
  month: {
    title: 'Месяц',
    active: false,
    label: 'year'
  },
  current_date: {
    title: 'Текущая дата',
    active: false,
    label: 'cur_date'
  },
  mo: {
    title: 'Выбор МО',
    active: false,
    label: 'mo'
  }
}

export let LISTTAG = {
  row: {
    type: 'row',
    tag: 'div',
    classN: 'row',
    orderby: 0,
    name: null,
    parent: null,
  },
  col_md: {
    type: 'col_md',
    tag: 'div',
    classN: '12',
    orderby: 0,
    name: null,
    parent: null,
  },
  span: {
    type: 'span',
    tag: 'div',
    classN: '',
    orderby: 0,
    name: null,
    parent: null,
    data: null,
    label: null,
    style: {},
  },
  table: {
    type: 'table',
    parent: null,
    data: null,
    cols: [
      {
        title: '№',
        pole: 'id',
        orderby: 1
      }
    ]
  },
  form: {
    type: 'form',
    name: null,
    parent: null,
    childs: []
  }
}

export const sort_orderBy = R.sortWith([
  R.descend(R.prop('orderby'))
]);
export const sort_orderBy_asc = R.sortWith([
  R.ascend(R.prop('orderby'))
]);