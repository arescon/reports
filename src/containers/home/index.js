import React from 'react';

import enhanceHome from './enhanceHome';
import AllReports from './AllReports';

const Home = ({ ready }) => 
  ready ? <AllReports all={true} /> : null;

export default enhanceHome(Home);