import { connect } from 'react-redux';

import compose from 'recompose/compose';
import lifecycle from 'recompose/lifecycle';
import withState from 'recompose/withState';
import withHandlers from 'recompose/withHandlers';

import * as R from 'ramda';
import moment from 'moment';

const enhance = compose(
  connect(
    (state) => ({
      state: state,
      error: state.home.error,
      _reports: state.menu.menu
    })
  ),
  withState('ready', 'ready_change', false),
  lifecycle({
    componentDidUpdate() {
      const { _reports, ready, ready_change } = this.props;
      if(_reports.length > 0 && !ready ) {
        ready_change(true);
      }
    }
  })
)

export default enhance;