// @flow
import React from 'react';
import { Link, Redirect } from 'react-router-dom';

import compose from 'recompose/compose';
import { withHandlers, withState } from 'recompose';

// import styles from './OrgRow.scss';

// const cx = classNames.bind(styles);

const enhance = compose(
  withState('redirect', 'changeRedirect', false),
)

const reportsRow = ({ id, reports_name, menu_name, redirect, changeRedirect }) => {
  if(redirect) return <Redirect to={`/otchet/${id}`} push />
    else return (
      <tr>
        <td>{reports_name}</td>
        <td>{menu_name}</td>
        <td>
          <Link to={`/otchet/${id}`}>просмотр</Link>
          <br />
          <a>печать</a>
        </td>
      </tr>
    )
};

export default enhance(reportsRow);
