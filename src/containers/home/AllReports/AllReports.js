import React from 'react';
import Sticky from 'react-sticky-el';
import classNames from 'classnames';

import styles from '../../styles.scss';
import tableStyles from '../../table.scss';

import ReportRow from './ReportRow';
import enhanceAllReports from './enhanceAllReports';

const OrgsList = ({ reports }) => {
  return [
    <Sticky key='ss1' stickyClassName={styles.sticky} className={classNames(styles.header, 'shadow')}>
      <table className={tableStyles.table}>
        <thead>
          <tr>
            <td>Название отчета</td>
            <td>Группировка</td>
            <td>Действия</td>
          </tr>
        </thead>
      </table>
    </Sticky>,
    <div key='ss2' className={styles.content}>
      <table className={tableStyles.table}>
        <tbody>
          {
            reports.map((rep,i) =>
              <ReportRow key={`p_${i}`} {...rep} />
          )}
        </tbody>
      </table>
    </div>
  ]
}

export default enhanceAllReports(OrgsList);