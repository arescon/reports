import { connect } from 'react-redux';

import compose from 'recompose/compose';
import lifecycle from 'recompose/lifecycle';
import withState from 'recompose/withState';

import * as R from 'ramda';

const enhance = compose(
  connect(
    state => ({
      roles: state.home.roles,
      _reports: state.menu.menu
    })
  ),
  withState('_all', 'changeAll', false),
  withState('reports', 'changeReports', []),

  lifecycle({
    componentDidMount() {
      const {
        current_menu, all, _reports,
        changeReports
      } = this.props
      if(all) changeReports(_reports);
      else {
          changeReports(R.filter((el)=> {
          return (el.url_name === current_menu);
        }, _reports))
      }
    },
  })
);

export default enhance;