import { connect } from 'react-redux';

import compose from 'recompose/compose';
import lifecycle from 'recompose/lifecycle';
import withState from 'recompose/withState';
import withHandlers from 'recompose/withHandlers';

import * as R from 'ramda';
import moment from 'moment';

const enhance = compose(
  connect(
    (state) => ({
      state: state,
      error: state.home.error
    })
  ),
  withState('current_menu', 'current_menu_change', null),
  withState('ready', 'ready_change', false),
  lifecycle({
    componentDidMount() {
      const { current_menu_change, ready_change, location } = this.props;
      current_menu_change(location.pathname.split('/')[1]);
      ready_change(true);
    },
    componentDidUpdate(prevProps) {
      const {
        current_menu, current_menu_change, ready_change,
        location, match, ready
      } = this.props;
      let url = location.pathname.split('/')[1];
      if(prevProps.current_menu !== url ) ready_change(false);
      if(prevProps.current_menu !== url && !ready) current_menu_change(url);
      if(prevProps.current_menu === url && !ready) ready_change(true);
    }
  })
)

export default enhance;
