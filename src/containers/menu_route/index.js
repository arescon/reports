import React from 'react';

import AllReports from '../home/AllReports';

import enhance from './enhance';

const Menu_route = ({ ready, current_menu }) =>
  ready ? <AllReports current_menu={current_menu} ready={ready} /> : null

export default enhance(Menu_route);