import React from 'react';
import { Link, Redirect } from 'react-router-dom';

import enhance from './enhance';

import style from 'src/containers/styles.scss';

const back = '/settings/menu';

const MenuOne = ({
  menu_one,
  menu_name,
  url_name,
  dbeg,
  dend,
  request,
  change,
  error,
  redirect
}) => {
  if (redirect) return <Redirect to={back}/>;
  return [
    <div className={style.header}>
      <Link to={back} >
        назад
      </Link>
    </div>,
    <div className={style.content}>
      <form onSubmit={request}>
        <div className='row'>
          <div className='col-md-3 form-group'>
            <label>
              ID
            </label>
            <input
              className='form-control'
              value={menu_one.id || 0}
              placeholder='ID'
              disabled
            />
          </div>
          <div className={`col-md-9 form-group ${error ? 'has-error' : ''}`}>
            <label className='control-label'>
              Название пункта { error ? error : '' }
            </label>
            <input
              className='form-control'
              placeholder={'Название пункта'}
              value={menu_name}
              name='menu_name'
              onChange={change}
            />
          </div>
          <div className='col-md-12 form-group'>
            <label>
              Ссылка
            </label>
            <input
              className='form-control'
              placeholder={'Ссылка'}
              value={url_name}
              name='url_name'
              onChange={change}
            />
          </div>
          <div className='col-md-6 form-group'>
            <label>
              Дата начала
            </label>
            <input
              className='form-control'
              placeholder={'Дата начала'}
              value={dbeg}
              type='date'
              name='dbeg'
              onChange={change}
            />
          </div>
          <div className='col-md-6 form-group'>
            <label>
              Дата окончания
            </label>
            <input
              className='form-control'
              placeholder={'Дата окончания'}
              value={dend}
              type='date'
              name='dend'
              onChange={change}
            />
          </div>
          <p className={style.divider} />
          <div className='col-md-12'>
            <button submit className='btn btn-success'>
              { menu_one.id ? 'Изменить' : 'Добавить' }
            </button>
          </div>
        </div>
      </form>
    </div>
  ]
}

export default enhance(MenuOne);