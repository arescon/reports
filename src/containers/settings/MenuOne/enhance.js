import { connect } from 'react-redux';

import compose from 'recompose/compose';
import lifecycle from 'recompose/lifecycle';
import withState from 'recompose/withState';
import withHandlers from 'recompose/withHandlers';

import * as R from 'ramda';
import moment from 'moment';

import { get_menu_one } from 'src/redux/actions/settings';

const d_f = 'YYYY-MM-DD';

const enhance = compose(
  connect(
    (state) => ({
      menu_one: state.settings.menu_one,
      page_settings: state.home.page_settings,
    })
  ),

  withState('menu_name', 'changeMenuName', ''),
  withState('url_name', 'changeUrlName', ''),
  withState('dbeg', 'changeDbeg', moment().format(d_f)),
  withState('dend', 'changeDend', moment().format(d_f)),
  withState('ready', 'changeReady', false),
  withState('error', 'changeError', null),
  withState('redirect', 'changeredirect', false),

  withHandlers({
    change: ({
      changeMenuName,
      changeUrlName,
      changeDbeg,
      changeDend
    }) => (e) => {
      const { name, value } = e.target;
      switch(name) {
        case 'menu_name': {
          changeMenuName(value);
          break;
        }
        case 'url_name': {
          changeUrlName(value);
          break;
        }
        case 'dbeg': {
          changeDbeg(value);
          break;
        }
        case 'dend': {
          changeDend(value);
          break;
        }
        default:
      }
    },
    request: ({menu_one, menu_name, url_name, dbeg, dend, changeError, changeredirect, changeReady}) => (e) => {
      e.preventDefault();
      changeError(null);
      Un.execUsp('reports_menu_reports_update', {
        aspoper: '_OPERID_',
        id: menu_one.id,
        menu_name: menu_name,
        url_name: url_name,
        dbeg: dbeg,
        dend: dend
      }, {}, function (ajd) {
        changeredirect(true)
      });
    },
    submit: ({}) => (e) => {
      e.preventDefault();
    }
  }),

  lifecycle({
    componentDidMount() {
      const { dispatch } = this.props;
      dispatch(get_menu_one(this.props.match.params.id))
    },
    componentDidUpdate() {
      const { ready, menu_one, changeMenuName, changeUrlName, changeDbeg, changeDend, changeReady } = this.props;
      if(!R.isEmpty(menu_one) && !ready) {
        changeMenuName(menu_one.menu_name);
        changeUrlName(menu_one.url_name);
        changeDbeg(moment(menu_one.dbeg).format(d_f));
        changeDend(moment(menu_one.dend).format(d_f));
        changeReady(true);
      }
    }
  })
)

export default enhance;