import React from 'react';
import Sticky from 'react-sticky-el';
import classNames from 'classnames';
import { Link } from 'react-router-dom';

import styles from 'src/containers/styles.scss';
import tableStyles from 'src/containers/table.scss';

import Row from './Row';

import enhance from './enhance';

type Props = {
  reports: Array,
  id_menu: int,
  _id_menu: int,
  page_settings: boolean,
  delete_report: (Event) => void,
  loading: boolean
}
const ReportsMenu = ({ reports, page_settings, delete_report, id_menu, loading, _id_menu }: Props) => {
  if(!page_settings) return 'Доступ запрещен';
  else {
    if(loading) return 'loading'; else return [
      <div key='s1' className={styles.header}>
        <Link to='/settings/menu' >
          назад
        </Link>
        <Link to={`/settings/rep_one/${_id_menu}/0`} style={{float: 'right'}} >
          Новый отчет
        </Link>
      </div>,
      <div key='s2' className={styles.content}>
        <Sticky stickyClassName={styles.sticky} className={classNames(styles.header, 'shadow')}>
          <table className={tableStyles.table}>
            <thead>
              <tr>
                <td>Название отчета</td>
                <td>Alias</td>
                <td>№</td>
                <td>Действия</td>
              </tr>
            </thead>
          </table>
        </Sticky>
        <div className={styles.content}>
          <table className={tableStyles.table}>
            <tbody>
              {
                reports.map((rep,i) => <Row
                  key={`p_${i}`}
                  delete_report={delete_report}
                  _id_menu={_id_menu} {...rep}
                />)
              }
            </tbody>
          </table>
        </div>
      </div>
    ]
  }
}

export default enhance(ReportsMenu);