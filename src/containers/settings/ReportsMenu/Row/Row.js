// @flow
import React from 'react';
import { Link, Redirect } from 'react-router-dom';

import style from 'src/containers/styles.scss';

import compose from 'recompose/compose';
import { withHandlers, withState } from 'recompose';

const enhance = compose(
  withState('redirect', 'changeRedirect', false),
)

const Row = ({ id, reports_name, alias_proc_name, _id_menu, delete_report, redirect, changeRedirect }) => {
  if(redirect) return <Redirect to={`/settings/rep_one/${_id_menu}/${id}`} push />
    else return (
    <tr>
      <td onClick={()=>changeRedirect(true)}>{reports_name}</td>
      <td>{alias_proc_name}</td>
      <td>{id}</td>
      <td>
        <Link to={`/settings/rep_one/${_id_menu}/${id}`}>Изменить</Link>
        <br />
        <a className={style.red} onClick={()=>delete_report(id)}>Удалить</a>
      </td>
    </tr>
  )
}

export default enhance(Row);
