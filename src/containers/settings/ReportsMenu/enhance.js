import { connect } from 'react-redux';

import compose from 'recompose/compose';
import lifecycle from 'recompose/lifecycle';
import withState from 'recompose/withState';
import withHandlers from 'recompose/withHandlers';

import { get_reports_menu, reset_reports } from 'src/redux/actions/settings';

const enhance = compose(
  connect(
    (state) => ({
      settings: state.settings,
      reports: state.settings.reports,
      page_settings: state.home.page_settings,
    })
  ),

  withState('loading', 'handlerLoader', true),
  withState('_id_menu', 'handlerIdMenu', true),

  lifecycle({
    componentDidMount() {
      const { dispatch, match, handlerLoader, handlerIdMenu } = this.props;
      let id_menu = match.params.id_menu;
      dispatch(reset_reports());
      dispatch(get_reports_menu(id_menu));
      handlerIdMenu(id_menu);
      handlerLoader(false);
    },
    componentWillUnmount() {
      const { dispatch, handlerLoader } = this.props;
      dispatch(reset_reports());
      handlerLoader(true);
    }
  }),

  withHandlers({
    delete_report: ({ dispatch, match }) => (id) => {
      Un.execUsp('reports_reports_proc_delete', {
        aspoper: '_OPERID_',
        id: id,
      }, {}, function () {
        dispatch(get_reports_menu(match.params.id_menu));
      });
    }
  })
)

export default enhance;