import { connect } from 'react-redux';

import compose from 'recompose/compose';
import lifecycle from 'recompose/lifecycle';

const enhance = compose(
  connect(
    (state) => ({
      settings: state.settings,
      page_settings: state.home.page_settings,
    })
  )
)

export default enhance;