import React from 'react';

import { Link } from 'react-router-dom';

import enhanceMain from './enhanceMain';

import style from '../../styles.scss';

type Props = {
  settings: Array,
  page_settings: boolean
}
const Main = ({ settings, page_settings }: Props) => {
  if(!page_settings) return 'Доступ запрещен';
  else return (
    <div className={style.content}>
      <Link to='/settings/menu' className='btn btn-primary' >Настройка меню</Link>
    </div>
  )
}

export default enhanceMain(Main);