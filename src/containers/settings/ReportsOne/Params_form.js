import React from 'react';
import Modal from 'react-responsive-modal';
import * as R from 'ramda';
import moment from 'moment';
import Resizable from 're-resizable';
import { toast } from 'react-toastify';

import style from 'src/containers/styles.scss';
import { LISTTAG, sort_orderBy, sort_orderBy_asc } from 'src/config';

const DeleteToName = props =>
  <span
    onClick={()=>props.deleteToName(props.id)}
    className={style.delete_btn}
    style={{ top: '0px' }}
  >
    x
  </span>

const SYZE_COL_MD = {
  12: 100,
  11: 91.66,
  10: 83.33,
  9: 75,
  8: 66.66,
  7: 58.33,
  6: 50,
  5: 41.66,
  4: 33.33,
  3: 25,
  2: 16.66,
  1: 8.33
}

export default class Params_form extends React.Component {
  state = {
    edited: null
  }
  changeModal(m_name) {
    this.setState({edited: m_name})
  }
  handlerChange(type, name, parent) {
    const { el, handlerRefresh } = this.props;
    let sort_by = sort_orderBy(el.childs);
    let obj = {...LISTTAG[type]};
    obj.name = moment().format('YYYY_MM_DD_x');
    obj.parent = parent;
    obj.orderby = sort_by[0] ? sort_by[0].orderby + 1 : 1;
    el.childs.push(obj);
    this.setState({
      edited: name
    })
    handlerRefresh();
  }
  handlerChangeInputs(ev, _el) {
    const { el, handlerRefresh } = this.props;
    let name = _el.name;
    let item = R.find(n => n.name === name, el.childs);
    let key = ev.target.dataset.key;
    item[key] = ev.target.value;
    this.setState({
      edited: name
    });
    handlerRefresh();
  }
  deleteToName(id) {
    const { el, handlerRefresh } = this.props;
    el.childs = R.filter(n => n.name !== id, el.childs);
    handlerRefresh();
  }
  changeSize(size, child) {
    const { el, handlerRefresh } = this.props;
    let f = R.find(R.propEq('name', child.name))(el.childs);
    f.classN = size;
    handlerRefresh();
  }
  renderChild(parent) {
    const { el } = this.props;
    let childs = R.filter(n => n.parent === parent, el.childs);
    return childs.map((item, i) => {
      switch (item.type) {
        case 'col_md':
          return <Resizable
              key={`sda_${i}`}
              size={{
                width: `${SYZE_COL_MD[item.classN]}%`
              }}
              enable= {{
                top:false,
                right:true,
                bottom:false,
                left:false,
                topRight:false,
                bottomRight:false,
                bottomLeft:false,
                topLeft:false
              }}
              style={{
                borderRight: '2px solid #05bb56'
              }}
              className={`${style.header} col-md-${item.classN}`}
              onResizeStop={(e, direction, ref, d) => {
                let cur_size = parseFloat(ref.style.width);
                let new_size;
                let k = R.keys(SYZE_COL_MD);
                let min = 100;
                k.map((k_el,i) => {
                  let k_min = Math.abs(SYZE_COL_MD[k_el] - cur_size);
                  min = min < k_min ? min : k_min;
                  new_size = min < k_min ? new_size : k_el;
                });
                this.changeSize(new_size, item);
              }}
            >
              { item.type }
              <DeleteToName
                id={item.name}
                deleteToName={(id)=>this.deleteToName(id)}
              />
              <div className='col-md-12'>
                { this.renderChild(item.name) }
                <ModalBlock
                  label='+'
                  ClassName={style.plus_block}
                  m_name={item.name}
                  changeModal={(m_name)=>this.changeModal(m_name)}
                  status={this.state.edited === item.name ? true : false}
                >
                  <div className='row'>
                    <div className='col-md-12'>
                      <h2>Добавить элемент</h2>
                    </div>
                    <div className='col-md-12'>
                      <div className='item form-group'>
                        <label>Span (Текст)</label>
                        <img
                          onClick={()=>{
                            this.handlerChange('span', null, item.name);
                          }}
                          className={`form-control ${style.btn_select_type}`}
                        />
                      </div>
                    </div>
                  </div>
                </ModalBlock>
              </div>
            </Resizable>
          break;
        case 'span':
          return (
            <div key={`sda_${i}`} className={`${style.header} col-md-12`}>
              <div className='col-md-12'>
                span
                <DeleteToName
                  id={item.name}
                  deleteToName={(id)=>this.deleteToName(id)}
                />
              </div>
              <ModalBlock
                label='Настройка'
                ClassName={style.plus_block}
                m_name={item.name}
                changeModal={(m_name)=>this.changeModal(m_name)}
                status={this.state.edited === item.name ? true : false}
              >
                <div className='row'>
                  <div className='col-md-12'>
                    <h2>Настройка элемента</h2>
                  </div>
                  <div className='col-md-12 form-group'>
                    <label htmlFor='abs_11'>Текст</label>
                    <input
                      key={`sda_${i}`}
                      id='abs_11'
                      type='text'
                      className='form-control'
                      value={item.label || ''}
                      data-key='label'
                      onChange={(e)=>this.handlerChangeInputs(e, item)}
                      placeholder='Текст'
                    />
                  </div>
                  <div className='col-md-12 form-group'>
                    <label htmlFor='abs_11'>Привязка к данным (оставить пустым если только текст)</label>
                    <input
                      key={`sda_${i}`}
                      id='abs_12'
                      type='text'
                      className='form-control'
                      value={item.data || ''}
                      data-key='data'
                      onChange={(e)=>this.handlerChangeInputs(e, item)}
                      placeholder='привязка к данным'
                    />
                  </div>
                </div>
              </ModalBlock>
            </div>
          )
          break;
        default:
      }
    });
  }
  render() {
    const { el } = this.props;
    return (
      <div>
        { this.renderChild(el.name) }
        <ModalBlock
          label='+'
          ClassName={style.plus_block}
          m_name={el.name}
          changeModal={(m_name)=>this.changeModal(m_name)}
          status={this.state.edited === el.name ? true : false}
        >
          <div className='row'>
            <div className='col-md-12'>
              <h2>Добавить элемент</h2>
            </div>
            <div className='col-md-12'>
              <div className='item form-group'>
                <label>Секция</label>
                <img
                  onClick={()=>{
                    this.handlerChange('col_md', null, el.name);
                  }}
                  className={`form-control ${style.btn_select_type}`}
                  src='/content/images/section.png'
                />
              </div>
            </div>
          </div>
        </ModalBlock>
      </div>
    )
  }
}

const ModalBlock = ({m_name, label, ClassName, status, changeModal, children}) => {
  return [
    <span
      key='m_1'
      className={ClassName || ''}
      onClick={()=>changeModal(m_name)}
      className={style.plus_btn_col}
    >
      { label || 'Настройка'}
    </span>,
    <Modal
      key='m_2'
      open={status}
      onClose={()=>changeModal(null)}
    >
      { children }
    </Modal>
  ]
}