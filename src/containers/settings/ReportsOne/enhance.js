import { connect } from 'react-redux';

import compose from 'recompose/compose';
import lifecycle from 'recompose/lifecycle';
import withState from 'recompose/withState';
import withHandlers from 'recompose/withHandlers';

import * as R from 'ramda';
import moment from 'moment';

import { get_reports_one, reset_report_one } from 'src/redux/actions/settings';
import { get_menu } from 'src/redux/actions/menu';

const d_f = 'YYYY-MM-DD';

const enhance = compose(
  connect(
    state => ({
      report_one: state.settings.report_one,
      page_settings: state.home.page_settings,
      services: state.home.serv,
      state: state,
    })
  ),
  
  withState('ready', 'changeReady', false),
  withState('redirect', 'changeredirect', false),
  withState('_id_menu', 'handlerIdMenu', true),
  withState('_id_report', 'handlerIdReport', 0),

  withState('reports_name', 'changeName', ''),
  withState('alias_proc_name', 'changeAlias', ''),
  withState('reports_service_pk', 'changeServicePk', null ),

  withState('config_form', 'changeConfigForm', null),
  withState('config_render', 'changeConfigRender', null),

  withState('dbeg', 'changeDbeg', moment().format(d_f)),
  withState('dend', 'changeDend', moment().format(d_f)),

  withHandlers({
    request: ({
      report_one, reports_name, match,
      alias_proc_name, dbeg, dend,
      config_form, config_render, reports_service_pk,
      changeredirect, dispatch
    }) => (e) => {
      e.preventDefault();
      if(match.params.id > 0) {
        Un.cud('put', 'reports_reports_proc_update', {
          aspoper: '_OPERID_',
          id: report_one.id,
          reports_name: reports_name,
          alias_proc_name: alias_proc_name,
          reports_service_pk: reports_service_pk,
          dbeg: dbeg,
          dend: dend,
          render_json: JSON.stringify(config_form),
          after_json: JSON.stringify(config_render),
        }, {}, {}, {
          s: () => {
            console.log('update ok');
            dispatch(reset_report_one());
            dispatch(get_menu());
            changeredirect(true);
          }
        });
      } else {
        Un.cud('post', 'reports_usp_reports_proc_insert', {
          aspoper: '_OPERID_',
          menu_reports_pk: match.params.id_menu,
          reports_name: reports_name,
          alias_proc_name: alias_proc_name,
          reports_service_pk: reports_service_pk,
          dbeg: dbeg,
          dend: dend,
          render_json: JSON.stringify(config_form),
          after_json: JSON.stringify(config_render),
        }, {}, {}, {
          s: () => {
            console.log('insert ok');
            dispatch(reset_report_one());
            dispatch(get_menu());
            changeredirect(true);
          }
        });
      };
    },
    change: ({
      changeName, changeAlias,
      changeDbeg, changeDend
    }) => (e) => {
      e.preventDefault();
      const { name, value } = e.target;
      switch(name) {
        case 'reports_name': {
          changeName(value);
          break;
        }
        case 'alias_proc_name': {
          changeAlias(value);
          break;
        }
        case 'alias_proc_name': {
          changeAlias(value);
          break;
        }
        case 'dbeg': {
          changeDbeg(value);
          break;
        }
        case 'dend': {
          changeDend(value);
          break;
        }
        default:
      }
    },
    service_change: ({
      changeServicePk
    }) => (e) => {
      e.preventDefault;
      changeServicePk(e.target.value);
    },
    changeConfig: ({
      changeConfigForm
    }) => (e) => {
      changeConfigForm(e)
    }
  }),

  lifecycle({
    componentWillMount() {
      const { dispatch, match, handlerIdMenu, handlerIdReport } = this.props;
      dispatch(reset_report_one());

      if(match.params.id > 0) {
        dispatch(get_reports_one(match.params.id, match.params.id_menu));
      }

      handlerIdMenu(match.params.id_menu);
      handlerIdReport(match.params.id);
    },
    componentDidUpdate() {
      const {
        ready, report_one, changeReady, match,
        changeName, changeAlias, changeDbeg, changeDend,
        changeConfigForm, changeConfigRender, changeServicePk
      } = this.props;
      if(match.params.id > 0 ) {
        if(!ready && !R.isEmpty(report_one)) {
          changeConfigForm(JSON.parse(report_one.render_json));
          changeConfigRender(JSON.parse(report_one.after_json));
          changeName(report_one.reports_name);
          changeAlias(report_one.alias_proc_name);
          changeDbeg(moment(report_one.dbeg).format(d_f));
          changeDend(moment(report_one.dend).format(d_f));
          changeServicePk(report_one.reports_service_pk);
          changeReady(true);
        }
      } else if(!ready) changeReady(true);
    },
    // componentWillUnmount() {
    //   const { dispatch } = this.props;
    //   dispatch(reset_report_one());
    // }
  })
)

export default enhance;