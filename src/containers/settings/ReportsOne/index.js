import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import Checkbox from 'react-simple-checkbox';
import Select2 from 'react-select2-wrapper';
import Modal from 'react-responsive-modal';
import classNames from 'classnames';
import * as R from 'ramda';
import moment from 'moment';
import Sticky from 'react-sticky-el';
import { toast } from 'react-toastify';

import style from 'src/containers/styles.scss';
import tableStyles from 'src/containers/table.scss';
import Input_Label from 'src/components/Label_input';
import { CONFIG, LISTTAG, sort_orderBy, sort_orderBy_asc } from 'src/config';

import Roles from './Roles';
import enhance from './enhance';

import Params_form from './Params_form';
import FormParams from './FormParams';

const Panel = props => 
  <div className={`${style.panel_react} panel panel-default`}>
    <div className="panel-body" style={{
      backgroundColor: '#ffffff',
      padding: '5px',
      paddingBottom: '5px'
    }}>
      { props.children }
    </div>
  </div>

const AddRow = props =>
  <div className={style.plus_block}>
    <span onClick={()=>props.add({
      type: 'row',
    })} className={style.plus_btn}>+</span>
  </div>

class AddCol extends React.Component {
  state = {
    modal: false
  }
  chageModal() {
    this.setState({
      modal: !this.state.modal
    })
  }
  render() {
    let props = this.props;
    return <div className='col-md-1'>
      <span
        onClick={()=>this.chageModal()}
        className={style.plus_btn_col}
      >+</span>
      <Modal open={this.state.modal} onClose={()=>this.chageModal()}>
        <div className='row'>
          <div className='col-md-12'>
            <h2>Добавить элемент</h2>
          </div>
          <div className='col-md-12'>
            <div className={`form-group ${style.item}`}>
              <label>Таблица</label>
              <img
                onClick={()=>{
                  props.add_child({
                    type: 'table'
                  }, {
                    name: props.id
                  })
                  this.chageModal()
                }}
                className={`form-control ${style.btn_select_type}`}
                src='/content/images/table_ico.png'
              />
            </div>
            <div className='item form-group'>
              <label>Форма</label>
              <img
                onClick={()=>{
                  props.add_child({
                    type: 'form'
                  }, {
                    name: props.id
                  })
                  this.chageModal()
                }}
                className={`form-control ${style.btn_select_type}`}
                src='/content/images/form_ico.png'
              />
            </div>
          </div>
        </div>
      </Modal>
    </div>
  }
}

class Params_table extends React.Component {
  state = {
    pole: '',
    title: '',
    total: false,
    modal: false,
  }
  chageModal() {
    this.setState({
      modal: !this.state.modal
    })
  }
  handlerChange(e, type) {
    let val = e.target.value;
    if(type === 'total') {
      val = !this.state.total
    }
    this.setState({
      [type]: val
    })
  }
  handlerChangeOld(e, type, _col) {
    const { el, handlerChangeOld } = this.props;
    handlerChangeOld(e, type, _col, el)
  }
  add_col_table() {
    const { el, add_col_table } = this.props;
    add_col_table(this.state.pole, this.state.title, this.state.total, el.name);
    this.setState({
      pole: '',
      title: '',
      total: false
    });
  }
  delete_col(orderby) {
    const { el, delete_col, handlerData } = this.props;
    delete_col(orderby, el.name);
  }
  render() {
    const { el, handlerData } = this.props;
    let cols = sort_orderBy_asc(el.cols);
    return <div>
      <span
        onClick={()=>this.chageModal()}
        className={style.plus_btn_col}
      >Настройка таблицы</span>
      <Modal
        open={this.state.modal}
        onClose={()=>this.chageModal()}
        center
      >
        <div className='row'>
          <div className='col-md-12'>
            <h2>Настройка таблицы</h2>
          </div>
          <div className='col-md-12'>
            <label>Данные</label>
            <input
              type='text'
              value={el.data || ''}
              onChange={(e)=>handlerData(el.name, e.target.value)}
              placeholder='Данные'
            />
          </div>
          <div className='col-md-12'>
            <Sticky stickyClassName={style.sticky} className={classNames(style.header, 'shadow')}>
              <table className={tableStyles.table}>
                <thead>
                  <tr>
                    <td>
                      Поле
                    </td>
                    <td>
                      Название поля
                    </td>
                    <td>
                      Итого показать?
                    </td>
                    <td>
                      Действия
                    </td>
                  </tr>
                </thead>
              </table>
            </Sticky>
            <div className={style.content}>
              <table className={tableStyles.table}>
                <tbody>
                  {
                    cols.map((c,i)=>{
                      return <tr key={`tr_${i}`}>
                        <td>
                          <input
                            value={c.pole}
                            onChange={(e)=> this.handlerChangeOld(e, 'pole', c)}
                            placeholder='Поле'
                          />
                        </td>
                        <td>
                          <input
                            value={c.title}
                            onChange={(e)=> this.handlerChangeOld(e, 'title', c)}
                            placeholder='Заголовок'
                          />
                        </td>
                        <td>
                          <input
                            checked={c.total}
                            type='checkbox'
                            onChange={(e)=> this.handlerChangeOld(e, 'total', c)}
                            placeholder='Итого'
                          />
                        </td>
                        <td>
                          <a className={style.red} onClick={()=>this.delete_col(c.orderby)}>Удалить</a>
                        </td>
                      </tr>
                    })
                  }
                  <tr style={{ background: '#d6d6d6' }}>
                    <td>
                      <input
                        value={this.state.pole}
                        onChange={(e)=>this.handlerChange(e, 'pole')}
                        placeholder='Поле'
                      />
                    </td>
                    <td>
                      <input
                        value={this.state.title}
                        onChange={(e)=>this.handlerChange(e, 'title')}
                        placeholder='Заголовок'
                      />
                    </td>
                    <td>
                      <input
                        checked={this.state.total}
                        type='checkbox'
                        onChange={(e)=>this.handlerChange(e, 'total')}
                        placeholder='Итого'
                      />
                    </td>
                    <td>
                      <a onClick={(even)=>{
                          even.preventDefault();
                          this.add_col_table();
                        }}
                      >Добавить</a>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div className='col-md-12'>
            <h5>Показ:</h5>
          </div>
          <div className='col-md-12'>
            <Sticky stickyClassName={style.sticky} className={classNames(style.header, 'shadow')}>
              <table className={tableStyles.table}>
                <thead>
                  <tr>
                    {
                      cols.map((c,i)=>{
                        return <td key={`th_${i}`}>{c.title}</td>
                      })
                    }
                  </tr>
                </thead>
              </table>
            </Sticky>
            <div className={style.content}>
              <table className={tableStyles.table}>
                <tbody>
                  <tr>
                    {
                      cols.map((c,i)=>{
                        return <td key={`td_${i}`}>{c.pole}</td>
                      })
                    }
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </Modal>
    </div>
  }
}

const DeleteToName = props =>
  <span
    onClick={()=>props.deleteToName(props.id)}
    className={style.delete_btn}
  >
    x
  </span>

class ConfigR extends React.Component {
  state = {
    modal: false,
    config: this.props.config_render || []
  }
  add(v, el, row) {
    let _conf = this.state.config;
    // if el === null then v.type add to and state.config
    let el_ads = {...LISTTAG[v.type]};
    el_ads.name = moment().format('YYYY_MM_DD_x');

    if(el && !row) {
      el_ads.parent = el.name
    }

    if(R.isNil(el_ads)) { // is null
      _conf.push(el_ads);
    } else { // is not null
      _conf.push(el_ads);
    }
    this.setState({
      config: _conf,
      modal: false
    })
  }
  handlerData(name, value) {
    console.log(name, value);
    let el = R.find(R.propEq('name', name))(this.state.config);
    el.data = value;
    this.setState(this.state);
  }
  handlerChangeOld(e, type, _col, _el) {
    let el = R.find(R.propEq('name', _el.name))(this.state.config);
    let col = R.find(R.propEq('orderby', _col.orderby))(el.cols);
    if(type === 'total') {
      col[type] = col[type] ? !col[type] : true;
    } else col[type] = e.target.value;
    this.setState(this.state);
  }
  add_col_table(pole, title, total, table_name) {
    let el = R.find(R.propEq('name', table_name))(this.state.config);
    console.log('DER43', el);
    let sort_by = sort_orderBy(el.cols);
    let obj = {
      pole: pole,
      title: title,
      total: total,
      orderby: sort_by[0] ? sort_by[0].orderby + 1 : 1
    };
    el.cols.push(obj);
    this.setState(this.state);
  }
  delete_col(orderby, table_name) {
    let el = R.find(R.propEq('name', table_name))(this.state.config);
    let sort_by = sort_orderBy(el.cols);
    let sortable = R.filter(n => n.orderby !== orderby, sort_by);
    let _new = [];
    R.forEach(n => {
      if(n.orderby > orderby) {
        n.orderby = n.orderby - 1
        _new.push(n);
      } else _new.push(n);
    }, sortable);
    el.cols = _new;
    this.setState(this.state);
  }
  deleteToName(id) {
    let arr = R.filter(n => n.name !== id, this.state.config);
    this.setState({
      config: R.filter(n => n.parent !== id, arr)
    })
  }
  updateConfig() {
    this.props.changeConfigRender(this.state.config);
    toast.success("Конфиг показа обновлен");
  }
  componentDidMount() {
    setInterval(() => {
      this.props.changeConfigRender(this.state.config);
    }, 60000); // 1 минута
  }
  componentWillUnmount() {
    clearInterval(this.interval);
  }
  render_form(name) {
    let arr = this.state.config;
    if(!R.isEmpty(arr)) {
      arr = R.filter(n=> n.parent === name,arr)
      return arr.map((el,i)=> {
        switch (el.type) {
          case 'row':
            return [
              <Panel key={`ei_${i}`}>
                <div className='col-md-12'>
                  row
                  <DeleteToName
                    id={el.name}
                    deleteToName={(id)=>this.deleteToName(id)}
                  />
                  
                    {
                      this.render_form(el.name)
                    }
                  
                </div>
                <AddCol
                  id={el.name}
                  add_child={(type, el)=>this.add(type, el)}
                  chageModal={()=>this.chageModal()}
                  modal={this.state.modal}
                />
              </Panel>,
              <AddRow add={(v)=>this.add(v, el, true)} key={`eb_${i}`} />
            ]
            break;
          case 'table':
            return (
              <Panel key={`ei_${i}`}>
                <div className='col-md-12'>
                  table
                  <DeleteToName
                    id={el.name}
                    deleteToName={(id)=>this.deleteToName(id)}
                  />
                  <Params_table
                    el={el}
                    handlerData={(n, v)=>this.handlerData(n,v)}
                    add_col_table={(r, t, tt, e)=>this.add_col_table(r, t, tt, e)}
                    handlerChangeOld={(e, t, c, el) => this.handlerChangeOld(e, t, c, el)}
                    delete_col={(o, e)=>this.delete_col(o, e)}
                  />
                </div>
              </Panel>
            )
            break;
          case 'form':
            return (
              <Panel key={`ei_${i}`}>
                <div className='col-md-12'>
                  form
                  <DeleteToName
                    id={el.name}
                    deleteToName={(id)=>this.deleteToName(id)}
                  />
                  <Params_form
                    el={el}
                    handlerRefresh={()=>this.setState(this.state)}
                  />
                </div>
              </Panel>
            )
            break;
          default:
        }
      })
    } else {
      return <AddRow add={(v)=>this.add(v, null, true)} />
    }
  }
  render() {
    return (
      <Panel>
        { this.render_form(null) }
        <br />
        <div className='col-md-12'>
          <input
            type='button'
            className='btn btn-info'
            onClick={()=>this.updateConfig()}
            value='Обновить конфигурацию'
          />
        </div>
      </Panel> 
    )
  }
}

const ReportsOne = ({
  redirect, config_form, config_render, _id_menu, _id_report,
  request, change, changeConfigForm, changeConfigRender,
  service_change, reports_name, alias_proc_name, dbeg, dend,
  ready, services, reports_service_pk
}) => {
  let new_arr = [];
  if(!R.isEmpty(services)) {
    services.map((el)=>{
      new_arr.push({
        id: el.id,
        text: el.id+' - '+el.service_name
      })
    })
  }
  let back = `/settings/reports/${_id_menu}`;
  if (redirect) return <Redirect to={back}/>;
  if(ready) return [
    <div key='s1' className={style.header}>
      <Link to={back} >
        назад
      </Link>
    </div>,
    <form key='s2' onSubmit={request}>
      <div className={style.content}>
        <div className='row'>
          <Input_Label
            width='3'
            label='ID'
            value={ _id_report }
            placeholder='ID'
            name='id'
            change={change}
            disabled
          />
          <Input_Label
            width='9'
            label='Название отчета'
            value={reports_name || ''}
            placeholder='Название отчета'
            name='reports_name'
            change={change}
          />
          <Input_Label
            width='4'
            label='Алиас Un'
            value={alias_proc_name || ''}
            placeholder='Алиас Un'
            name='alias_proc_name'
            change={change}
          />
          <Input_Label
            width='4'
            label='Доступ с'
            value={dbeg || ''}
            placeholder='с'
            name='dbeg'
            type='date'
            change={change}
          />
          <Input_Label
            width='4'
            label='по'
            value={dend || ''}
            placeholder='по'
            name='dend'
            type='date'
            change={change}
          />
          <div className='col-md-4'>
            <label className='control-label'>
              Сервис
            </label>
            <Select2
              className='form-control'
              data={new_arr}
              value={reports_service_pk}
              onSelect={service_change}
              options={{
                placeholder: 'Выберите сервис',
              }}
            />
          </div>
        </div>
      </div>
      <div className={style.content}>
        <div className='row'>
          <div className='col-md-12 form-horizontal'>
            настройки по показу формы
            <FormParams
              _config={config_form}
              changeConfigForm={changeConfigForm}
            />
          </div>
        </div>
      </div>
      <div className={style.content}>
        <div className='row'>
          <div className='col-md-12'>
            настройки по Ролям:
            <Roles
              _id_report={_id_report}
            />
          </div>
        </div>
      </div>
      <div className={style.content}>
        <div className='row'>
          <div className='col-md-12'>
            настройки по показу результата
            <ConfigR
              config_render={config_render}
              changeConfigRender={changeConfigRender}
              ready={ready}
            />
          </div>
        </div>
      </div>
      <div className={style.content}>
        <div className='row'>
          <div className='col-md-12'>
            <button submit='true' className='btn btn-success'>
              { _id_report > 0 ? 'Изменить' : 'Добавить' }
            </button>
          </div>
        </div>
      </div>
    </form>
  ]; else return false;
};

export default enhance(ReportsOne);