// @flow
import React from 'react';

import * as R from 'ramda';
import moment from 'moment';

import style from 'src/containers/styles.scss';

const d_f = 'DD.MM.YYYY';

const Row = ({
  id, roles_pk, _roles, dbeg, dend, _delete, openModal
}) => {
  return (
    <tr>
      <td>{R.find(R.propEq('id', roles_pk))(_roles).text}</td>
      <td>{moment(dbeg).format(d_f)}</td>
      <td>{moment(dend).format(d_f)}</td>
      <td>
        <a onClick={()=>openModal(id)} >
          Редактировать
        </a>
        <a className={style.red}
          onClick={()=>_delete(id)}
        >
          Удалить
        </a>
      </td>
    </tr>
  )
};


export default Row;
