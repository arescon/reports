import { connect } from 'react-redux';

import compose from 'recompose/compose';
import lifecycle from 'recompose/lifecycle';
import withState from 'recompose/withState';
import withHandlers from 'recompose/withHandlers';

import * as R from 'ramda';
import moment from 'moment';

const d_f = 'YYYY-MM-DD';

const enhance = compose(
  connect(
    (state) => ({
      _roles: state.home.roles
    })
  ),

  withState('role', 'changeRole', null),
  withState('dbeg', 'changeDbeg', moment().format(d_f)),
  withState('dend', 'changeDend', moment().format(d_f)),

  withState('roles', 'changeRoles', []),
  withState('modal', 'handlerModal', false),
  withState('refresh', 'changeRefresh', false),
  withState('ready', 'changeReady', false),
  
  withState('id_role_edit', 'handlerEditIdRole', 0),

  withHandlers({
    openModal: ({
      handlerModal, handlerEditIdRole,
      changeRole, changeDbeg, changeDend,
      roles
    }) => (id) => {
      handlerEditIdRole(id);
      if(id > 0) {
        let role = R.find(R.propEq('id', id))(roles);
        changeRole(role.roles_pk);
        changeDbeg(moment(role.dbeg).format(d_f));
        changeDend(moment(role.dend).format(d_f));
        handlerModal(false);
      } else {
        changeRole(null);
        changeDbeg(moment().format(d_f));
        changeDend(moment().format(d_f));
      }
      handlerModal(true);
    },
    closeModal: ({
      handlerModal
    }) => () => {
      handlerModal(false);
    },
    change: ({
      changeDbeg, changeDend
    }) => (e) => {
      e.preventDefault();
      const { name, value } = e.target;
      switch(name) {
        case 'dbeg': {
          changeDbeg(value);
          break;
        }
        case 'dend': {
          changeDend(value);
          break;
        }
        default:
      }
    },
    roles_change: ({
      changeRole
    }) => (e) => {
      e.preventDefault;
      changeRole(e.target.value);
    },
    add_Role: ({
      role, dbeg, dend, _id_report, id_role_edit,
      changeRole, changeDbeg, changeDend,
      changeRefresh, handlerModal
    }) => (e) => {
      e.preventDefault;
      if(id_role_edit > 0) {
        Un.execUsp('reports_proc_roles_update', {
          id: id_role_edit,
          reports_proc_pk: _id_report,
          aspoper: '_OPERID_',
          roles_pk: role,
          dbeg: dbeg,
          dend: dend
        }, {}, ()=>{
          changeRole(null);
          changeDbeg(moment().format(d_f));
          changeDend(moment().format(d_f));
          changeRefresh(true);
          handlerModal(false);
        });
      } else {
        Un.execUsp('reports_proc_roles_insert', {
          reports_proc_pk: 
          ,
          aspoper: '_OPERID_',
          roles_pk: role,
          dbeg: dbeg,
          dend: dend
        }, {}, () => {
          changeRole(null);
          changeDbeg(moment().format(d_f));
          changeDend(moment().format(d_f));
          changeRefresh(true);
          handlerModal(false);
        });
      }
    },
    delete_role: props => (id_role) => {
      Un.execUsp('reports_proc_roles_delete', {
        aspoper: '_OPERID_',
        id: id_role,
      }, {}, function () {
        props.changeRefresh(true);
      });
    },
    get_data: ({
      changeRoles,
      changeRefresh,
    }) => (id, cb) => {
      Un.execUsp('reports_proc_roles_select', {
        reports_proc_pk: id,
        aspoper: '_OPERID_',
        substr: '',
        page_num: 1,
        page_size: null
      }, {}, (res) => {
        changeRefresh(false);
        changeRoles(res.Data);
        cb ? cb() : null;
      }, ()=>{
        changeRefresh(false);
        changeRoles([]);
        cb ? cb() : null;
      });
    },
  }),
  
  lifecycle({
    componentWillMount() {
      const { _id_report, get_data, changeReady } = this.props;
      get_data(_id_report, ()=> {
        changeReady(true)
      });
    },
    componentDidUpdate() {
      if(this.props.ready) if(this.props.refresh) this.props.get_data(this.props._id_report)
    }
  })
);
export default enhance;