import React from 'react';
import Select2 from 'react-select2-wrapper';
import classNames from 'classnames';
import Sticky from 'react-sticky-el';
import Modal from 'react-responsive-modal';

import * as R from 'ramda';

import styles from 'src/containers/styles.scss';
import tableStyles from 'src/containers/table.scss';

import enhance from './enhance';

import Row from './Row';

import Input_Label from 'src/components/Label_input';

const Roles = ({
  roles, _roles, role, dbeg, dend, modal, id_role_edit,
  roles_change, add_Role, delete_role,
  change, openModal, closeModal
}) => {
  let new_arr = [];
  if(!R.isEmpty(_roles)) {
    _roles.map((el)=>{
      new_arr.push({
        id: el.id,
        text: el.id+' - '+el.title
      })
    })
  }
  return [
    <div key='rol_11'>
      <input type='button' className='btn btn-info' onClick={(e)=>{
          e.preventDefault;
          openModal(0)
        }
      } value='Добавить роль' />
    </div>,
    <div key='rol_2' className='row'>
      <div className={`col-md-12 ${styles.top_m_5}`} />
      <div className='col-md-12 '>
        <Sticky stickyClassName={styles.sticky} className={classNames(styles.header, 'shadow')}>
          <table className={tableStyles.table}>
            <thead>
              <tr>
                <td>Роль</td>
                <td>Доступ с</td>
                <td>по</td>
                <td>Действия</td>
              </tr>
            </thead>
          </table>
        </Sticky>
        <div className={styles.content}>
          <table className={tableStyles.table}>
            <tbody>
              {
                roles.map((role,i) => <Row
                  key={`p_${i}`}
                  _roles={new_arr}
                  openModal={openModal}
                  _delete={delete_role}
                  {...role}
                />)
              }
            </tbody>
          </table>
          <Modal
            open={modal}
            onClose={closeModal}
            center
          >
            <h2>{id_role_edit > 0 ? `Изменение роли ` : `Добавление роли`}</h2>
            <div className='row'>
              <div className='col-md-4 form-group'>
                <label className='control-label'>
                  Выбрать роль
                </label>
                <Select2
                  className='form-control'
                  data={new_arr}
                  value={role}
                  onSelect={roles_change}
                  options={{
                    placeholder: 'Поиск по ролям',
                  }}
                />
              </div>
              <Input_Label
                width='4'
                label='Доступ с'
                value={ dbeg }
                placeholder=''
                name='dbeg'
                type='date'
                change={change}
              />
              <Input_Label
                width='4'
                label='по'
                value={ dend }
                placeholder=''
                name='dend'
                type='date'
                change={change}
              />
              <div className='col-md-12'>
                <input
                  type='button'
                  onClick={add_Role}
                  className='btn btn-info'
                  value={
                    id_role_edit > 0 ? 'Изменить' : 'Добавить'
                  }
                />
              </div>
            </div>
          </Modal>
        </div>
      </div>
    </div>
  ]
}

export default enhance(Roles);