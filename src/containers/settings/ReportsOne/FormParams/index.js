import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import Checkbox from 'react-simple-checkbox';
import Select2 from 'react-select2-wrapper';
import Modal from 'react-responsive-modal';
import classNames from 'classnames';
import * as R from 'ramda';
import moment from 'moment';
import Sticky from 'react-sticky-el';
import { toast } from 'react-toastify';
import ModalButton from 'src/components/modal/button';

import styles from 'src/containers/styles.scss';
import tableStyles from 'src/containers/table.scss';
import Input_Label from 'src/components/Label_input';

import enhance from './enhance';

import RowParameters from './row';

import EditParams from './edit';

const FormParams = ({
  _config, changeConfigForm,
  id_modal,
  handlerResetParams,
  handlerDeleteRow
}) => {
  let conf = _config || {};
  let key = Object.keys(conf);
  return <div className='row'>
    <div className='col-md-6'>
      <ModalButton
        label='Добавить параметр'
        id={'new_other_param'}
        classN='btn-info'
        styleN={{}}
      >
        <EditParams
          element={{}}
          _config={_config}
          changeConfigForm={changeConfigForm}
        />
      </ModalButton>
      <button
        className='btn btn-danger'
        onClick={handlerResetParams}
        style={{
          marginLeft: '5px'
        }}
      >
        Очистить параметры
      </button>
    </div>
    <div className={`col-md-12 ${styles.top_m_5}`} />
    <div className='col-md-12'>
      <table className={tableStyles.table}>
        <thead className={classNames(styles.header, 'shadow')}>
          <tr>
            <td>Имя параметра API</td>
            <td>Тег</td>
            <td>Тип тега</td>
            <td>Действия</td>
          </tr>
        </thead>
        <tbody className={styles.content_table}>
          {
            key.map((k, i) => <RowParameters
              key={`p_${i}`}
              _delete={handlerDeleteRow}
              id_param={k}
              _config={_config}
              changeConfigForm={changeConfigForm}
              parametr={_config[k]}
            />)
          }
        </tbody>
      </table>
    </div>
  </div>
}

export default enhance(FormParams);