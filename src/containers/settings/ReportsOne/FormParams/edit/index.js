import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import Checkbox from 'react-simple-checkbox';
import Select2 from 'react-select2-wrapper';
import Modal from 'react-responsive-modal';
import classNames from 'classnames';
import * as R from 'ramda';
import moment from 'moment';
import Sticky from 'react-sticky-el';
import { toast } from 'react-toastify';

import { DICTIONS } from 'src/config';

import enhance from './enhance';

const EditParams = ({
  parametr, id_param,
  toggleModal,
  handlerOnChange, handlerOnChangeSettings,
  handlerSave,
  helper
}) => {
  return [
    <h2 key='s1' className=''>
      {
        id_param? 'Добавление' : 'Редактирование'
      } параметра
    </h2>,
    <div key='s2' className='row'>
      <div className='col-md-6'>
        <div className='form-group'>
          <label>Имя параметра</label>
          <input
            type='text'
            className='form-control'
            placeholder='Имя параметра'
            value={parametr.title || ''}
            onChange={(ev)=>handlerOnChange('title', ev)}
          />
        </div>
      </div>
      <div className='col-md-6'>
        <div className='form-group'>
          <label>Binding</label>
          <input
            type='text'
            className='form-control'
            placeholder='Связь с данными'
            value={parametr.bind || ''}
            onChange={(ev)=>handlerOnChange('bind', ev)}
          />
        </div>
      </div>
      <div className='col-md-6'>
        <div className='form-group'>
          <label>Выберите тег</label>
          <Select2
            className='form-control' 
            data={[
              {
                id: 'input',
                text: 'input',
              },
              {
                id: 'select',
                text: 'select',
              },
              {
                id: 'dictionary',
                text: 'Справочник',
              },
              {
                id: 'search',
                text: 'Справичник (Поиск)',
              }
            ]}
            options={{
              placeholder: 'Выберите тег',
            }}
            value={ parametr.tag || null }
            onChange={(ev)=>handlerOnChange('tag', ev)}
          />
        </div>
      </div>
    </div>,
    parametr.tag ? <div key='s4' className='row'>
        <div className='col-md-12'>
          Параметры
        </div>
        {
          parametr.tag == 'input' ?
            [
              <div key='d1' className='col-md-6'>
                <label>Выберите тип</label>
                <Select2
                  className='form-control' 
                  data={[
                    {
                      id: 'text',
                      text: 'Текстовое',
                    },
                    {
                      id: 'integer',
                      text: 'Числовое',
                    },
                    {
                      id: 'date',
                      text: 'Дата',
                    },
                  ]}
                  options={{
                    placeholder: 'Выберите тип',
                  }}
                  value={ parametr.params ? parametr.params.type || null : null }
                  onChange={(ev)=>handlerOnChangeSettings('type', ev)}
                />
              </div>,
              <div key='d2' className='col-md-6'>
                <label>placeholder</label>
                <input
                  type='text'
                  className='form-control'
                  placeholder='placeholder'
                  value={ parametr.params ? parametr.params.placeholder || '' : ''}
                  onChange={(ev)=>handlerOnChangeSettings('placeholder', ev)}
                />
              </div>
            ]
          : null
        }
        {
          parametr.tag == 'select' ?
            [
              <div key='f1' className='col-md-12'>
                <label>API URL</label>
                <input
                  type='text'
                  className='form-control'
                  placeholder='api_url'
                  value={ parametr.params ? parametr.params.api_url || '' : ''}
                  onChange={(ev)=>handlerOnChangeSettings('api_url', ev)}
                />
              </div>,
              <div key='f2' className='col-md-6'>
                <label>API id</label>
                <input
                  type='text'
                  className='form-control'
                  placeholder='api_id'
                  value={ parametr.params ? parametr.params.api_id || '' : ''}
                  onChange={(ev)=>handlerOnChangeSettings('api_id', ev)}
                />
              </div>,
              <div key='f3' className='col-md-6'>
                <label>API val</label>
                <input
                  type='text'
                  className='form-control'
                  placeholder='api_val'
                  value={ parametr.params ? parametr.params.api_val || '' : ''}
                  onChange={(ev)=>handlerOnChangeSettings('api_val', ev)}
                />
              </div>,
            ]
          : null
        }
        {
          parametr.tag == 'dictionary' ?
            [
              <div key='h1' className='col-md-12'>
                <label></label>
                <Select2
                  className='form-control' 
                  data={ DICTIONS }
                  options={{
                    placeholder: 'Выберите справочник',
                  }}
                  value={ parametr.params ? parametr.params.dic || null : null }
                  onChange={(ev)=>handlerOnChangeSettings('dic', ev)}
                />
              </div>
            ]
          : null
        }
        {
          parametr.tag == 'search' ?
          [
            <div key='f1_1a' className='col-md-9'>
              <label>API URL</label>
              <input
                type='text'
                className='form-control'
                placeholder='api_url'
                value={ parametr.params ? parametr.params.api_url || '' : ''}
                onChange={(ev)=>handlerOnChangeSettings('api_url', ev)}
              />
            </div>,
            <div key='f1_1b' className='col-md-3'>
              <div className='form-group form-check'>
                <input
                  type='checkbox'
                  className='form-check-input'
                  placeholder='api_un'
                  data-check={true}
                  checked={ parametr.params ? parametr.params.api_un : ''}
                  onChange={(ev)=>handlerOnChangeSettings('api_un', ev)}
                />
                <label className='form-check-label'>UN</label>
              </div>
            </div>,
            <div key='f2_1' className='col-md-6'>
              <label>API id</label>
              <input
                type='text'
                className='form-control'
                placeholder='api_id'
                value={ parametr.params ? parametr.params.api_id || '' : ''}
                onChange={(ev)=>handlerOnChangeSettings('api_id', ev)}
              />
            </div>,
            <div key='f3_1' className='col-md-6'>
              <label>API val</label>
              <input
                type='text'
                className='form-control'
                placeholder='api_val'
                value={ parametr.params ? parametr.params.api_val || '' : ''}
                onChange={(ev)=>handlerOnChangeSettings('api_val', ev)}
              />
            </div>,
          ]
          : null
        }
      </div>
    : null,
    <div key='s5' className='col-md-12' style={{
      marginTop: '10px',
      marginBottom: '10px'
    }}>
      <button
        type='button'
        className='btn btn-success'
        style={{
          marginRight: '5px'
        }}
        onClick={handlerSave}
      >
        Сохранить
      </button>
      <button
        type='button'
        className='btn btn-secondary'
        onClick={()=>toggleModal(null)}
      >
        Отмена
      </button>
    </div>
  ]
}

export default enhance(EditParams);