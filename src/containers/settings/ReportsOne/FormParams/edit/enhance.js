import { connect } from 'react-redux';

import compose from 'recompose/compose';
import lifecycle from 'recompose/lifecycle';
import withState from 'recompose/withState';
import withHandlers from 'recompose/withHandlers';

import * as R from 'ramda';
import moment from 'moment';

import { toggleModal } from 'actions/modal';

const enhance = compose(
  connect(
    state => ({
      id_modal: state.modal.id
    }),
    dispatch => ({
      toggleModal: (id) => dispatch(toggleModal(id))
    })
  ),
  withState('parametr','handlerChangeParametr', {}),
  withState('_id_param','handlerChangeIdParametr', null),
  withHandlers({
    handlerOnChange: ({ parametr, handlerChangeParametr}) => (field, ev) => {
      let par = {...parametr};
      ev.preventDefault();
      let v = ev.target.value;
      if(field === 'tag') {
        if(par[field] !== v) {
          par.params ? par.params = {} : null;
        }
      }
      par[field] = v;
      handlerChangeParametr(par);
    },
    handlerOnChangeSettings: ({ parametr, handlerChangeParametr}) => (field, ev) => {
      ev.preventDefault();
      let par = {...parametr};
      let s = 'params';
      par.params ? null : par[s] = {};
      if(ev.target.dataset.check) {
        par.params[field] = par.params[field] ? !par.params[field] : true
      } else par.params[field] = ev.target.value;
      handlerChangeParametr(par);
    },
    handlerSave: ({ 
      changeConfigForm, toggleModal,
      _config, parametr, _id_param, id_param
    }) => ev => {
      ev.preventDefault();
      let c = {..._config};
      c[_id_param] = parametr;
      changeConfigForm(c);
      toggleModal(null);
    }
  }),
  lifecycle({
    componentDidMount() {
      const {
        handlerChangeParametr, handlerChangeIdParametr,
        _config, element, id_param
      } = this.props;
      handlerChangeParametr(element || {})
      let conf = _config || {};
      let last_id = R.last(Object.keys(conf));
      handlerChangeIdParametr( id_param || parseInt(last_id) + 1 || 1 );
    },
    componentDidUpdate() {
      // console.log('FT6TU', this.props);
    }
  })
)

export default enhance;