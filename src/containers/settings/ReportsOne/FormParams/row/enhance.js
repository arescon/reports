import { connect } from 'react-redux';

import compose from 'recompose/compose';
import lifecycle from 'recompose/lifecycle';
import withState from 'recompose/withState';
import withHandlers from 'recompose/withHandlers';

import * as R from 'ramda';
import moment from 'moment';

const enhance = compose(
  lifecycle({
    componentDidMount() {
      // console.log(this.props);
    }
  })
)

export default enhance;