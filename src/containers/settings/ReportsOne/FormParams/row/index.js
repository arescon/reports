// @flow
import React from 'react';
import ModalButton from 'src/components/modal/button';

import Enhance from './enhance';

import EditParams from '../edit';

const Row = ({
  id_param, _config, changeConfigForm,
  parametr, _delete
}) => {
  let { tag, params, title } = parametr;
  return (
    <tr>
      <td>
        { title || '' }
      </td>
      <td>
        { tag || '' }
      </td>
      <td>
        { params ? params.type || '' : '' }
      </td>
      <td>
        <div className='btn-group btn-group-sm' role='group'>
          <ModalButton
            label='Ред.'
            id={`edit_param_${id_param}`}
            classN='btn-info'
            styleN={{}}
          >
            <EditParams
              element={parametr}
              _config={_config}
              id_param={id_param}
              changeConfigForm={changeConfigForm}
            />
          </ModalButton>
          <button
            className='btn btn-danger'
            onClick={()=>_delete(id_param)}
          >
            Удалить
          </button>
        </div>
      </td>
    </tr>
  )
};

export default Enhance(Row);