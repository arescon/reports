import { connect } from 'react-redux';

import compose from 'recompose/compose';
import lifecycle from 'recompose/lifecycle';
import withState from 'recompose/withState';
import withHandlers from 'recompose/withHandlers';

import * as R from 'ramda';
import moment from 'moment';

import { toggleModal } from 'actions/modal';

const enhance = compose(
  connect(
    state => ({
      id_modal: state.modal.id
    }),
    dispatch => ({
      toggleModal: (id) => dispatch(toggleModal(id))
    })
  ),
  withHandlers({
    handlerResetParams: ({ _config, changeConfigForm }) => ev => {
      ev.preventDefault();
      _config = {};
      changeConfigForm(_config);
    },
    handlerDeleteRow: ({ _config, changeConfigForm }) => id => {
      let conf = R.omit([id], ..._config);
      changeConfigForm(conf);
    }
  }),
)

export default enhance;