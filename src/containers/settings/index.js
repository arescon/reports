import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import Main from './Main';
import Menu from './Menu';
import MenuOne from './MenuOne';

import ReportsMenu from './ReportsMenu';
import ReportsOne from './ReportsOne';

const Settings = () => {
  return (
    <Switch>
      <Route path="/settings/main" component={Main} />
      <Route path="/settings/menu/:id" component={MenuOne} />
      <Route path="/settings/menu" component={Menu} />
      <Route path="/settings/rep_one/:id_menu/:id" component={ReportsOne} />
      <Route path="/settings/reports/:id_menu" component={ReportsMenu} />
      <Redirect to="/settings/main" />
    </Switch>
  )
}

export default Settings;