// @flow
import React from 'react';

type Props = {
  id: number,
  title: string,
  parentID: number,
  checked: number
};

const RoleRow = ({ id, title, parentID, checked }: Props) => {
  return (
    <tr>
      <td>{title}</td>
      <td>{id}</td>
      <td>{parentID}</td>
      <td>{checked}</td>
    </tr>
  )
}


export default RoleRow;
