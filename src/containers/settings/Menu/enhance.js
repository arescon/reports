import { connect } from 'react-redux';

import compose from 'recompose/compose';
import lifecycle from 'recompose/lifecycle';
import withState from 'recompose/withState';
import withHandlers from 'recompose/withHandlers';

import { get_menu, reset_menu } from 'src/redux/actions/settings';

const enhance = compose(
  connect(
    (state) => ({
      menu: state.settings.menu,
      page_settings: state.home.page_settings,
    })
  ),

  withState('loading', 'handlerLoader', true),

  withHandlers({
    delete_menu: ({ dispatch }) => (id) => {
      Un.execUsp('reports_menu_reports_delete', {
        aspoper: '_OPERID_',
        id: id,
      }, {}, function () {
        dispatch(get_menu())
      });
    }
  }),

  lifecycle({
    componentDidMount() {
      const { dispatch, handlerLoader } = this.props;
      dispatch(reset_menu());
      dispatch(get_menu());
      handlerLoader(false);
    },
    componentWillUnmount() {
      const { dispatch, handlerLoader } = this.props;
      dispatch(reset_menu());
      handlerLoader(true);
    }
  })
)

export default enhance;