// @flow
import React from 'react';
import { Link, Redirect } from 'react-router-dom';

import style from 'src/containers/styles.scss';

import compose from 'recompose/compose';
import { withHandlers, withState } from 'recompose';

const enhance = compose(
  withState('redirect', 'changeRedirect', false),
)

const Row = ({ id, menu_name, url_name, delete_menu, redirect, changeRedirect }) => {
  if(redirect) return <Redirect to={`/settings/reports/${id}`} push />
    else return (
      <tr>
        <td onClick={()=>changeRedirect(true)}>{menu_name}</td>
        <td>{url_name}</td>
        <td>{id || 'NaN'}</td>
        <td>
          <Link to={`/settings/menu/${id}`}>Изменить</Link>
          <br />
          <Link to={`/settings/reports/${id}`}>список отчетов</Link>
          <br />
          <a className={style.red} onClick={()=>delete_menu(id)}>Удалить</a>
        </td>
      </tr>
)}

export default enhance(Row);
