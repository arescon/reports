import React from 'react';
import Sticky from 'react-sticky-el';
import classNames from 'classnames';
import { Link } from 'react-router-dom';

import styles from '../../styles.scss';
import tableStyles from '../../table.scss';

import Row from './Row';

import enhance from './enhance';

type Props = {
  menu: Array,
  loading: boolean,
  page_settings: boolean,
  delete_menu: (Event) => void,
}
const Menu = ({ menu, loading, page_settings, delete_menu }: Props) => {
  if(!page_settings) return 'Доступ запрещен menu';
  else {
    if(loading) return 'loading'; else return [
      <div key='menu1' className={styles.header}>
        <Link to='/settings' >
          назад
        </Link>
        <Link to='/settings/menu/0' style={{float: 'right'}} >
          Добавить пункт
        </Link>
      </div>,
      <div key='menu2' className={styles.content}>
        <Sticky
          stickyClassName={styles.sticky}
          className={classNames(styles.header, 'shadow')}
        >
          <table className={tableStyles.table}>
            <thead>
              <tr>
                <td>Название пункта</td>
                <td>Ссылка</td>
                <td>№</td>
                <td>Действия</td>
              </tr>
            </thead>
          </table>
        </Sticky>
        <div className={styles.content}>
          <table className={tableStyles.table}>
            <tbody>
              {menu.map((rep,i) => {
                return <Row
                  key={`p_${i}`} delete_menu={delete_menu} {...rep}
                />
              })}
            </tbody>
          </table>
        </div>
      </div>
    ]
  }
}

export default enhance(Menu);