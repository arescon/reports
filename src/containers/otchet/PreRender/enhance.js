import { connect } from 'react-redux';
import * as R from 'ramda';
import { toast } from "react-toastify";

import compose from 'recompose/compose';
import lifecycle from 'recompose/lifecycle';
import withState from 'recompose/withState';
import withHandlers from 'recompose/withHandlers';

import { getOtchet } from 'src/redux/actions/home';

const enhance = compose(
  connect(
    state => ({
      mo: state.home.mo,
      services: state.home.serv,
      res_otchet: state.home.res_otchet
    })
  ),
  withState('values', 'changeParams', {}),
  withHandlers({
    request: ({}) => (e) => {
      e.preventDefault();
    },
    submit: ({
      pre_json,
      values, changeParams,
      name, alias, service,
      services,
      dispatch,
      changeRender
    }) => (e) => {
      e.preventDefault();
      let error = {};
      let _params = values;
      if(!R.isEmpty(error)) {
        toast.error(`Некоторые поля не заполнены: ${JSON.stringify(error)}`);
      } else {
        let s = R.find(R.propEq('id', service))(services)
        if(s.id === 99) { // un
          Un.execUsp(alias, _params, {},
            (res) => {
              changeParams({});
              dispatch(getOtchet(res));
              changeRender();
            },
            (e_ur, xhr) => { // callbacks.e(p_url, jqXHR, textStatus, errorThrown, errInNotify);
              toast.error(xhr.responseJSON.Message || 'Ошибка');
            });
        } else if(s.id == 98) { // multy un
          Un.execUspMultiSet(alias, _params, {},
            (res) => {
              changeParams({});
              dispatch(getOtchet(res));
              changeRender();
          });
        } else if(s.id === 1 || s.id === 3 || s.id === 2) { // printsrv
          let k = Object.keys(_params);
          let m = '?';
          let len = k.length;
          k.map((el,i) => {
            m = m + `${el}=${_params[el]}${(len === i+1) ? '' : '&'}`
          })
          window.open(`rm/${s.service_url}/${alias}${m}`);
        }
      }
    },
    handlerParams: ({
      values, 
      changeParams
    }) => (e) => {
      let { name, value } = e.target;
      values[name] = value;
      changeParams(values);
    },
    handlerSelect: ({
      values, 
      changeParams
    }) => (e) => {
      let { name, value } = e.target;
      values[name] = value;
      changeParams(values);
    }
  }),
  lifecycle({
    componentDidMount() {
      let { changeParams, value } = this.props;
      changeParams({});
    }
  })
);

export default enhance;