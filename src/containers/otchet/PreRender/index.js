import React from 'react';
import Select2 from 'react-select2-wrapper';
import { DICTIONS } from 'src/config';

import moment from 'moment';
import { toast } from 'react-toastify';

import * as R from 'ramda';

import styles from 'src/containers/styles.scss';
import enhance from './enhance';

const Config_c = (props) => {
  const {
    pre_json, values, mo,
    submit, handlerParams, handlerSelect
  } = props;
  let arr_render = R.keys(pre_json);
  let _mo = [];
  if(!R.isEmpty(mo)) {
    mo.map((el)=>{
      _mo.push({
        id: el.ID,
        text: el.Nam_mok
      })
    })
  }
  return (
    <form
      onSubmit={submit}
      className='row'
    >
      <div className='col-md-12'>
        {
          arr_render.map((el,i) => {
            let t = pre_json[el];
            let p = t.params || {};
            if(t.tag) {
              switch (t.tag) {
                case 'input':
                  return <div key={`in_${i}`} className='col-md-6 form-group'>
                      <label>{t.title || ''}</label>
                      <input
                        type={p.type || 'text'}
                        name={t.bind}
                        className='form-control'
                        value={values[t.bind] || ''}
                        placeholder={p.placeholder || ''}
                        onChange={handlerParams}
                      />
                  </div>
                  break;
                case 'dictionary':
                  // DICTIONS
                  let dic = R.find(R.propEq('id', p.dic))(DICTIONS);
                  let arr = [];
                  props[dic.redux].map((el)=>{
                    arr.push({
                      id: el[dic.api_id],
                      text: el[dic.api_val]
                    })
                  })
                  return <div key={`dic_${i}`} className='col-md-6 form-group'>
                      <label>{dic.text || 'Выберите'}</label>
                      <Select2
                        className='form-control' 
                        data={arr}
                        options={{
                          placeholder: dic.text || 'Выберите',
                        }}
                        value={values[t.bind] || ''}
                        name={t.bind}
                        onChange={handlerParams}
                      />
                    </div>;
                  break;
                default:
                  break;
              }
            } else return null;
          })
        }
        <div className='col-md-12'>
          <input
            type='submit'
            className={styles.btn_info_2}
            value='Сформировать отчет'
          />
        </div>
      </div>
    </form>
  )
}

const PreRender = (props) =>
  <Config_c
    {...props}
  />


export default enhance(PreRender);