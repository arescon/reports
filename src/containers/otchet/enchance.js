import { connect } from 'react-redux';

import compose from 'recompose/compose';
import lifecycle from 'recompose/lifecycle';
import withState from 'recompose/withState';
import withHandlers from 'recompose/withHandlers';

import * as R from 'ramda';

import { toast } from "react-toastify";

import { changeFullApp } from 'src/redux/actions/home';

const enhance = compose(
  connect(
    state => ({
      error: state.home.error,
      page_settings: state.home.page_settings,
      menu: state.menu.menu
    })
  ),
  
  withState('preRender', 'changePreRender', true),
  withState('ready', 'changeReady', false),

  withState('name', 'changeName', ''),
  withState('alias', 'changeAlias', ''),
  withState('service', 'changeService', null),
  withState('pre_json', 'changePreJson', {}),
  withState('render_json', 'changeRenderJson', {}),

  withHandlers({
    changeRender: ({
      preRender,
      changePreRender
    }) =>() => {
      changePreRender(!preRender);
    },
    getData: ({
      match, menu,
      changeName, changeAlias, changeService,
      changePreJson, changeRenderJson
    }) => () => {
      const _id = parseInt(match.params.id);
      const _menu = menu;
      let item = R.find(R.propEq('id', _id))(_menu);
      Un.execUsp('reports_reports_proc_select', {
        id: item.id,
        menu_reports_pk: item.menu_reports_pk,
        aspoper: '_OPERID_',
        substr: '',
        page_num: 1,
        page_size: null
      }, {}, (res) => {
        let r = res.Data[0];
        changeName(r.reports_name || '');
        changeAlias(r.alias_proc_name || '');
        changeService(r.reports_service_pk || null)
        changePreJson(r.render_json ? JSON.parse(r.render_json) : {});
        changeRenderJson(r.after_json ? JSON.parse(r.after_json) : {})
      }, ()=>{
        toast.error('Ошибка загрузки отчета');
      })
    }
  }),

  lifecycle({
    componentDidMount() {
      const { dispatch, getData, changeReady } = this.props;
      dispatch(changeFullApp());
      getData();
      changeReady(true);
    },
    componentWillUnmount() {
      const { dispatch } = this.props;
      dispatch(changeFullApp());
    }
  })
);

export default enhance;