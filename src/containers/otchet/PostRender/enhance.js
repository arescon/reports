import { connect } from 'react-redux';
import * as R from 'ramda';
import { toast } from "react-toastify";

import compose from 'recompose/compose';
import lifecycle from 'recompose/lifecycle';
import withState from 'recompose/withState';
import withHandlers from 'recompose/withHandlers';

const enhance = compose(
  connect(
    state => ({
      mo: state.home.mo,
      services: state.home.serv,
      res_otchet: state.home.res_otchet
    })
  ),
  withState('ready', 'changeReady', false),
  withState('params', 'changeParams', {}),
  withState('test', 'changeTest', [1,2,3]),
  withHandlers({
  }),
  lifecycle({
    componentDidMount() {
      const { render_json, ready, changeReady } = this.props;
      if(!R.isEmpty(render_json) && !ready ) {
        changeReady(true);
      }
    },
    componentDidUpdate() {
      const { render_json, ready, changeReady } = this.props;
      if(!R.isEmpty(render_json) && !ready ) {
        changeReady(true);
      }
    }
  }),
);

export default enhance;