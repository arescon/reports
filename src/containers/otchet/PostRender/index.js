import React from 'react';
import Sticky from 'react-sticky-el';
import classNames from 'classnames';
import * as R from 'ramda';

import style from 'src/containers/styles.scss';
import tableStyles from 'src/containers/table.scss';
import enhance from './enhance';

// функция глубокого поиска по объекту
function getPropByString(obj, propString) {
  if (!propString)
    return obj;
    var prop, props = propString.split(',');
    for (var i = 0, iLen = props.length - 1; i < iLen; i++) {
      prop = props[i];
      var candidate = obj[prop];
      if (candidate !== undefined) {
        obj = candidate;
      } else {
        break;
      }
    }
    return obj[props[i]];
}

// res_otchet - данные
const RenderConfig = (props) => {
  let { res_otchet, render_json, parent } = props;
  if(!R.isEmpty(render_json)) {
    let arr = R.filter(n=> n.parent === parent, render_json);
    return arr.map((el,i)=> {
      switch (el.type) {
        case 'form':
          return <div className='form-inline' key={`el.type_${i}`}>
            <RenderConfig
              res_otchet={res_otchet}
              render_json={el.childs}
              parent={el.name}
            />
          </div>;
          break;
        case 'span':
          let d = el.data ? getPropByString(res_otchet , el.data) : null;
          return <div key={`el.type_1_${i}`}>
              {
                el.label ? <h3 className=''>
                  { el.label }:
                  {
                    el.data ? <span style={{
                        marginLeft: '10px',
                        borderBottom: '1px solid'
                      }}
                    >
                      { d }
                    </span> : el.label ? el.data ? '...' : null : null
                  }
                </h3> : null
              }
            </div>
          break;
        case 'row':
        case 'col_md':
          return <div key={`el.type_${i}`} className={`col-md-${el.classN || 12}`} >
            <RenderConfig
              res_otchet={res_otchet}
              render_json={render_json}
              parent={el.name}
            />
          </div>
          break;
        case 'table':
          let arr_data = getPropByString(res_otchet , el.data); // res_otchet
          arr_data = Array.isArray(arr_data) ? arr_data : [];
          let f = {};
          let _cols = R.sort(R.ascend(R.prop('orderby')), el.cols) || [];
          console.log(_cols)
          return <div key={`el.type_${i}`} className='col-md-12'>
            <div className={classNames(style.header, 'shadow')}>
              <table className={tableStyles.table}>
                <thead>
                  <tr>
                    {
                      _cols.map((c,i)=>{
                        return <td key={`tr_${i}`}>             
                            { c.title || 'NaN'}
                          </td>
                      })
                    }
                  </tr>
                </thead>
              </table>
            </div>
            <div className={style.content}>
              <table className={tableStyles.table}>
                <tbody>
                  {
                    arr_data.map((row,i)=>{
                      return <tr key={`tr_${i}`}>
                        {
                          _cols.map((c,i2)=>{
                            c.total ? f[c.pole] ? f[c.pole] = f[c.pole] + row[c.pole] : f[c.pole] = row[c.pole] : null;
                            return <td key={`tr_${i2}`}>
                                { row[c.pole] }
                              </td>
                          })
                        }
                      </tr>
                    })
                  }
                  <tr style={{
                    background: '#c1c1c1'
                  }}>
                    {
                      _cols.map((c,i2)=>{
                        return <td  key={`trs_${i2}`}>
                          { f[c.pole] || '' }
                        </td>
                      })
                    }
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          break;
        default:
      }
    })
  }
}

const PostRender = (props) =>
  props.ready ?
    <div className='col-md-12'>
      <RenderConfig
        res_otchet={props.res_otchet}
        render_json={props.render_json}
        parent={null}
      />
    </div>
    : null;

export default enhance(PostRender);