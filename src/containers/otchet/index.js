import React from 'react';
import { Link } from 'react-router-dom';
import ReactToPrint from "react-to-print";
import * as R from 'ramda';

import enchance from './enchance';

import styles from '../styles.scss';

import PreRender from './PreRender';
import PostRender from './PostRender';

class Otchet extends React.Component {
  render() {
    const {
      preRender, ready, pre_json, render_json,
      name, alias, service,
      changeRender, 
    } = this.props;
    return ready ?
      [
        <div key='d1' className={styles.header}>
          <Link to={`/home`}>
            Все отчеты
          </Link>
          {
            !preRender ?
              [
                <a
                  key='ss'
                  onClick={()=> changeRender()}
                  style={{ float: 'right' }}>
                  Назад
                </a>,
                <ReactToPrint
                  key='sss'
                  trigger={() => <a style={{ float: 'right', marginRight: '10px' }} onClick={(e)=>e.preventDefault()}>Печать</a>}
                  content={() => this.componentRef}
                />
              ]
              : null
          }
          
        </div>,
        <div
          key='d2'
          ref={el => (this.componentRef = el)}
          className={styles.content + ' col-md-12'}
        >
          <div className='col-md-12'>
            <div className='col-md-12'>
              <h1 style={{ textAlign: 'center' }}>
                { name || '' }
              </h1>
            </div>
            {
              preRender ?
                <PreRender
                  pre_json={pre_json}
                  changeRender={changeRender}
                  name={name}
                  alias={alias}
                  service={service}
                />
                : <PostRender
                  render_json={render_json}
                  changeRender={changeRender}
                  name={name}
                  alias={alias}
                  service={service}
                />
            }
          </div>
        </div>
      ]
    : null
  }
}

export default enchance(Otchet)