import React from 'react';
import { Switch, Route, Redirect, Link } from 'react-router-dom';
import { ToastContainer } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

import enchanceApp from './enchanceApp';

import Home from './containers/home';
import Settings from './containers/settings';
import Otchet from './containers/otchet';

import { reportList, closeReports, refreshReports } from './style.scss';

import Menu_route from './containers/menu_route';

const Menu = (props) => {
  const indexes = [];
  props.menu.forEach((el)=> {
    if(indexes.indexOf(el.menu_reports_pk) < 0) {
      indexes.push(el.menu_reports_pk)
    }
  });
  return indexes.map((el, i)=>{
    let s = props.menu.find((e)=>{
      return e.menu_reports_pk === el
    })
    return (
      <Link
        key={`m_i_${i}`}
        className={`list-group-item ${props.current_menu===s.url_name ? 'active' : null }`}
        to={`/${s.url_name}`}
      >
        <p>{s.menu_name}</p>
      </Link>
    )
  })
}

const Settings_menu = (props) => {
  if(props.page_settings) return <Link
      className={`list-group-item ${props.current_menu==='settings' ? 'active' : null }`}
      to={`/settings`}
    >
      <p>Настройки</p>
    </Link>; else return null;
}

const Home_menu = (props) => 
  <Link className={`list-group-item ${props.current_menu==='home' ? 'active' : null }`} to={`/home`}>
    <p>Все отчеты</p>
  </Link>;

const App = ({
  ready, page_settings, menu, mo, full_app, current_menu,
  handlerCloseReports, handlerRefreshReports
}) =>
  ready ? <div className='mis-report'>
      <div className={`${reportList} noprint`}>
        <div className={closeReports} onClick={handlerCloseReports}>Закрыть</div>
        <div className={refreshReports} onClick={handlerRefreshReports}>Обновить отчеты</div>
        <div className='row'>
          <div className='col-md-12'>
            <h1 className='title'>Отчеты</h1>
          </div>
        </div>
        <div className='row'>
          {
            full_app ?
              <div className='col-md-3'>
                <div className="list-group" data-bind="foreach: Reports">
                  <Home_menu current_menu={current_menu} />
                  <Menu current_menu={current_menu} menu={menu} />
                  <Settings_menu current_menu={current_menu} page_settings={page_settings} />
                </div>
              </div> : null 
          }

          <div className={`col-md-${full_app ? '9' : '12'}`}>
            <Switch>
              <Route path="/home" component={Home} exact />
              <Route path="/otchet/:id" component={Otchet} exact />
              <Route path="/settings" component={Settings} />

              <Route path="/:menu_route" component={Menu_route} />

              <Redirect to="/home" />
            </Switch>
            <h5 className='title'>
              •••••
            </h5>
          </div>
        </div>
      </div>
      <ToastContainer autoClose={4000} />
    </div> : null

export default enchanceApp(App)