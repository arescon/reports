import { connect } from 'react-redux';
import compose from 'recompose/compose';

import { toggleModal } from 'actions/modal';

export default compose(
  connect(
    state => ({
      id_modal: state.modal.id
    }),
    dispatch => ({
      toggleModal: (id) => dispatch(toggleModal(id))
    })
  )
)