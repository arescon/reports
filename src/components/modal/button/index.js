import React from 'react';
import Modal from 'react-responsive-modal';

import enhance from './enhance';
/*
  Использование:
  
  import ModalButton from 'app/comps/modals/button';

  <ModalButton
    label='Ред.'
    id={'edit_user' + item.id}
    classN='btn-info'
    styleN={{}}
    onOpen={()=>{
      console.log('e');
    }}
  >
    header
  </ModalButton>
*/

const ModalButton = ({
  label, children, id, styleN, classN,
  toggleModal, id_modal, onOpen
}) => {
  let status = id_modal === id ? true : false,
    _class = classN || 'btn-success',
    _style = styleN || {
      marginBottom: '5px'
    }
  
  return [
    <button
      key='1'
      className={ 'btn ' + _class }
      style={ _style }
      onClick={e=> {
        e.preventDefault();
        onOpen ? onOpen() : null;
        toggleModal(id);
      }}
    >
      { label || '+'}
    </button>,
    <Modal
      key='2'
      center
      styles={{
        modal: {
          width: '100%',
          maxWidth: '800px'
        }
      }}
      open={status}
      onClose={()=> toggleModal(null) }
    >
      { children || null }
    </Modal>
  ]
}

export default enhance(ModalButton);