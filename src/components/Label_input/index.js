import React from 'react';

const Input_Label = ({
  width, label, value, placeholder,
  change, disabled, name, type
}) => {
  let _disabled = disabled ? {'disabled' : 'disabled'} : {};
  return (
    <div className={`col-md-${ width || '12'} form-group`}>
      <label className='control-label' htmlFor={`is_${name}`}>
        {label || <p style={{color: 'white'}}>-</p>}
      </label>
      <input
        id={`is_${name}`}
        className='form-control'
        placeholder={placeholder || 'Введите значение'}
        value={value}
        name={name}
        type={type || 'text'}
        onChange={change}
        {..._disabled}
      />
    </div>
  )
}

export default Input_Label;